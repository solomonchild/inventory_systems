package org.home.vb.inventorysystem;


import org.home.vb.inventorysystem.features.eoq.EOQ;
import org.junit.Assert;
import org.junit.Test;

import java.security.InvalidParameterException;


public class EOQWithoutDiscountTest {

    @Test
    public void basicModel() throws Exception {
            EOQ eoq = EOQ.newBuilder()
                    .setDemandPerYear(3600)
                    .setOrderCostPerOrder(200)
                    .setUnitHoldingCostPerYear(25)
                    .setUnitCostWithoutDiscount(100)
                    .build();

        Assert.assertEquals(240, eoq.getOptimalQuantity(), 0.1);
        Assert.assertEquals(240, eoq.getMaximumInventory(), 0.1);
        Assert.assertEquals(0, eoq.getMaximumBackorder(), 0.1);
        Assert.assertEquals(0.0667, eoq.getOrderIntervalPerYear(), 0.1);
        Assert.assertEquals(0, eoq.getROP(), 0.0);
        Assert.assertEquals(3000, eoq.getTotalOrderingCost(), 0.1);
        Assert.assertEquals(eoq.getTotalOrderingCost(), eoq.getTotalHoldingCost(), 0.1);
        Assert.assertEquals(360000, eoq.getTotalMaterialCost(), 0.1);

    }

    @Test
    public void basicModelWithLeadTime() throws Exception {
        EOQ eoq = EOQ.newBuilder()
                .setDemandPerYear(3600)
                .setOrderCostPerOrder(200)
                .setUnitHoldingCostPerYear(25)
                .setUnitCostWithoutDiscount(100)
                .setLeadTime(0.01)
                .build();

        Assert.assertEquals(240, eoq.getOptimalQuantity(), 0.1);
        Assert.assertEquals(240, eoq.getMaximumInventory(), 0.1);
        Assert.assertEquals(0, eoq.getMaximumBackorder(), 0.1);
        Assert.assertEquals(0.0667, eoq.getOrderIntervalPerYear(), 0.1);
        Assert.assertEquals(36, eoq.getROP(), 0.0);
        Assert.assertEquals(3000, eoq.getTotalOrderingCost(), 0.1);
        Assert.assertEquals(eoq.getTotalOrderingCost(), eoq.getTotalHoldingCost(), 0.1);
        Assert.assertEquals(360000, eoq.getTotalMaterialCost(), 0.1);

    }

    @Test
    public void withDeficit() throws Exception {
        EOQ eoq = EOQ.newBuilder()
                .setDemandPerYear(3600)
                .setOrderCostPerOrder(200)
                .setUnitHoldingCostPerYear(25)
                .setUnitShortageCostPerYear(40)
                .setUnitCostWithoutDiscount(100)
                .build();
        Assert.assertEquals(305.94, eoq.getOptimalQuantity(), 0.01);
        Assert.assertEquals(188.27, eoq.getMaximumInventory(), 0.01);
        Assert.assertEquals(117.67, eoq.getMaximumBackorder(), 0.01);
        Assert.assertEquals(0.085, eoq.getOrderIntervalPerYear(), 0.01);
        Assert.assertEquals(-117.67, eoq.getROP(), 0.01);
        Assert.assertEquals(2353.394, eoq.getTotalOrderingCost(), 0.01);
        Assert.assertEquals(1448.242, eoq.getTotalHoldingCost(), 0.01);
        Assert.assertEquals(905.15, eoq.getTotalShortageCost(), 0.01);
        Assert.assertEquals(360000, eoq.getTotalMaterialCost(), 0.01);

    }
    @Test
    public void withDeficitAndLeadTime() throws Exception {
        EOQ eoq = EOQ.newBuilder()
                .setDemandPerYear(3600)
                .setOrderCostPerOrder(200)
                .setUnitHoldingCostPerYear(25)
                .setUnitShortageCostPerYear(40)
                .setLeadTime(0.05)
                .setUnitCostWithoutDiscount(100)
                .build();
        Assert.assertEquals(305.94, eoq.getOptimalQuantity(), 0.01);
        Assert.assertEquals(188.27, eoq.getMaximumInventory(), 0.01);
        Assert.assertEquals(117.67, eoq.getMaximumBackorder(), 0.01);
        Assert.assertEquals(0.085, eoq.getOrderIntervalPerYear(), 0.01);
        Assert.assertEquals(62.33, eoq.getROP(), 0.01);
        Assert.assertEquals(2353.394, eoq.getTotalOrderingCost(), 0.01);
        Assert.assertEquals(1448.242, eoq.getTotalHoldingCost(), 0.01);
        Assert.assertEquals(905.15, eoq.getTotalShortageCost(), 0.01);
        Assert.assertEquals(360000, eoq.getTotalMaterialCost(), 0.01);

    }




    @Test
    public void withReplenishmentRate() throws Exception {
        EOQ eoq = EOQ.newBuilder()
                .setDemandPerYear(3600)
                .setOrderCostPerOrder(200)
                .setUnitHoldingCostPerYear(25)
                .setUnitCostWithoutDiscount(100)
                .setReplenishmentRatePerYear(3700)
                .build();
        Assert.assertEquals(1459.863, eoq.getOptimalQuantity(), 0.01);
        Assert.assertEquals(39.45, eoq.getMaximumInventory(), 0.01);
        Assert.assertEquals(0.0, eoq.getMaximumBackorder(), 0.0);
        Assert.assertEquals(0.4055, eoq.getOrderIntervalPerYear(), 0.01);
        Assert.assertEquals(0, eoq.getROP(), 0.01);
        Assert.assertEquals(493.1969, eoq.getTotalOrderingCost(), 0.01);
        Assert.assertEquals(eoq.getTotalOrderingCost(), eoq.getTotalHoldingCost(), 0.01);
        Assert.assertEquals(360000, eoq.getTotalMaterialCost(), 0.01);
    }

    @Test
    public void withReplenishmentRateAndLeadTime() throws Exception {
        EOQ eoq = EOQ.newBuilder()
                .setDemandPerYear(3600)
                .setOrderCostPerOrder(200)
                .setUnitHoldingCostPerYear(25)
                .setUnitCostWithoutDiscount(100)
                .setReplenishmentRatePerYear(3700)
                .setLeadTime(0.005)
                .build();
        Assert.assertEquals(1459.863, eoq.getOptimalQuantity(), 0.01);
        Assert.assertEquals(39.45, eoq.getMaximumInventory(), 0.01);
        Assert.assertEquals(0.0, eoq.getMaximumBackorder(), 0.0);
        Assert.assertEquals(0.4055, eoq.getOrderIntervalPerYear(), 0.01);
        Assert.assertEquals(18, eoq.getROP(), 0.01);
        Assert.assertEquals(493.1969, eoq.getTotalOrderingCost(), 0.01);
        Assert.assertEquals(eoq.getTotalOrderingCost(), eoq.getTotalHoldingCost(), 0.01);
        Assert.assertEquals(360000, eoq.getTotalMaterialCost(), 0.01);
    }
    @Test
    public void copyCtorWithReplenishmentRateAndNoDeficit() throws Exception {
        EOQ temp = EOQ.newBuilder()
                .setDemandPerYear(3600)
                .setOrderCostPerOrder(200)
                .setUnitHoldingCostPerYear(25)
                .setUnitCostWithoutDiscount(100)
                .setReplenishmentRatePerYear(3700)
                .setLeadTime(0.005)
                .build();
        EOQ eoq = temp.builderFromThis().setOptimalQuantity(1459.863).build();
        Assert.assertEquals(1459.863, eoq.getOptimalQuantity(), 0.01);
        Assert.assertEquals(39.45, eoq.getMaximumInventory(), 0.01);
        Assert.assertEquals(0.0, eoq.getMaximumBackorder(), 0.0);
        Assert.assertEquals(0.4055, eoq.getOrderIntervalPerYear(), 0.01);
        Assert.assertEquals(18, eoq.getROP(), 0.01);
        Assert.assertEquals(493.1969, eoq.getTotalOrderingCost(), 0.01);
        Assert.assertEquals(eoq.getTotalOrderingCost(), eoq.getTotalHoldingCost(), 0.01);
        Assert.assertEquals(360000, eoq.getTotalMaterialCost(), 0.01);
    }

    @Test(expected = InvalidParameterException.class)
    public void withReplenishmentRateAndInvalidLeadTime() throws Exception {
        EOQ eoq = EOQ.newBuilder()
                .setDemandPerYear(3600)
                .setOrderCostPerOrder(200)
                .setUnitHoldingCostPerYear(25)
                .setUnitCostWithoutDiscount(100)
                .setReplenishmentRatePerYear(3700)
                .setLeadTime(0.05)
                .build();
    }

    @Test(expected=InvalidParameterException.class)
    public void withReplenishmentRateAndDeficitAndInvalidLeadTime() throws Exception {
        EOQ eoq = EOQ.newBuilder()
                .setDemandPerYear(3600)
                .setOrderCostPerOrder(200)
                .setUnitHoldingCostPerYear(25)
                .setUnitCostWithoutDiscount(100)
                .setUnitShortageCostPerYear(40)
                .setReplenishmentRatePerYear(3700)
                .setLeadTime(0.05)
                .build();
    }

    @Test
    public void emptyEOQTest() {
        EOQ eoq = EOQ.newBuilder().build();
        Assert.assertEquals(Double.NaN, eoq.getOptimalQuantity(), 0.01);
    }

}
