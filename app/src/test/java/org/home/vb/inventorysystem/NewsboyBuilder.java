package org.home.vb.inventorysystem;

import junit.framework.Assert;

import org.home.vb.inventorysystem.features.newsboy.Newsboy;
import org.junit.Test;

import java.security.InvalidParameterException;

public class NewsboyBuilder {

    @Test
    public void setValidMean() throws Exception {
        double mean = 100;
        Newsboy newsboy = Newsboy.newBuilder().setOptimalServiceLevel(1).setStdDev(1).setMean(mean).build();
        Assert.assertEquals(mean, newsboy.getMean());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidMean() throws Exception {
        double mean = -1;
        Newsboy newsboy = Newsboy.newBuilder().setMean(mean).build();
    }

    @Test
    public void setValidStdDev() throws Exception {
        double stdDev = 100;
        Newsboy newsboy = Newsboy.newBuilder().setOptimalServiceLevel(1).setStdDev(stdDev).build();
        Assert.assertEquals(stdDev, newsboy.getStdDev());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidStdDev() throws Exception {
        double stdDev = 0;
        Newsboy.newBuilder().setStdDev(stdDev).build();
    }

    @Test
    public void setValidOrderCost() throws Exception {
        double orderCost = 100;
        Newsboy newsboy = Newsboy.newBuilder().setOptimalServiceLevel(1).setStdDev(1).setOrderCost(orderCost).build();
        Assert.assertEquals(orderCost, newsboy.getOrderCost());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidOrderCost() throws Exception {
        double orderCost = -1;
        Newsboy.newBuilder().setOrderCost(orderCost).build();
    }

    @Test
    public void setValidAcqCost() throws Exception {
        double cost = 100;
        Newsboy newsboy = Newsboy.newBuilder().setOptimalServiceLevel(1).setStdDev(1).setAcqCost(cost).build();
        Assert.assertEquals(cost, newsboy.getAcqCost());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidAcqCost() throws Exception {
        double cost = -1;
        Newsboy.newBuilder().setAcqCost(cost).build();
    }

    @Test
    public void setValidUnitSellingPrice() throws Exception {
        double sellingPrice = 100;
        Newsboy newsboy = Newsboy.newBuilder().setOptimalServiceLevel(1).setStdDev(1).setUnitSellingPrice(sellingPrice).build();
        Assert.assertEquals(sellingPrice, newsboy.getUnitSellingPrice());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidUnitSellingPrice() throws Exception {
        double sellingPrice = -1;
        Newsboy.newBuilder().setUnitSellingPrice(sellingPrice).build();
    }

    @Test
    public void setValidShortageCost() throws Exception {
        double shortageCost = 100;
        Newsboy newsboy = Newsboy.newBuilder().setOptimalServiceLevel(1).setStdDev(1).setShortageCost(shortageCost).build();
        Assert.assertEquals(shortageCost, newsboy.getShortageCost());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidShortageCost() throws Exception {
        double shortageCost = -1;
        Newsboy.newBuilder().setShortageCost(shortageCost).build();
    }

    @Test
    public void setValidSalvageValue() throws Exception {
        double salvageValue = 100;
        Newsboy newsboy = Newsboy.newBuilder().setOptimalServiceLevel(1).setStdDev(1).setSalvageValue(salvageValue).build();
        Assert.assertEquals(salvageValue, newsboy.getSalvageValue());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidSalvageValue() throws Exception {
        double salvageValue = -1;
        Newsboy.newBuilder().setSalvageValue(salvageValue).build();
    }

    @Test
    public void setValidOptimalOrderQuantity() throws Exception {
        double optimalOrder = 100;
        Newsboy newsboy = Newsboy.newBuilder().setOptimalServiceLevel(1).setStdDev(1).setOptimalOrderQuantity(optimalOrder).build();
        Assert.assertEquals(optimalOrder, newsboy.getOptimalOrderQuantity());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidOptimalOrderQuantity() throws Exception {
        double optimalOrder = -1;
        Newsboy.newBuilder().setOptimalOrderQuantity(optimalOrder).build();
    }

    @Test
    public void setValidOptimalServiceLevel() throws Exception {
        double optimalServiceLevel = 1;
        Newsboy newsboy = Newsboy.newBuilder().setStdDev(1).setOptimalServiceLevel(optimalServiceLevel).build();
        Assert.assertEquals(optimalServiceLevel, newsboy.getOptimalServiceLevel());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidOptimalServiceLevelNegative() throws Exception {
        double optimalServiceLevel = -1;
        Newsboy.newBuilder().setOptimalServiceLevel(optimalServiceLevel).build();
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidOptimalServiceLevelLarge() throws Exception {
        double optimalServiceLevel = 1.1;
        Newsboy.newBuilder().setOptimalServiceLevel(optimalServiceLevel).build();
    }
}
