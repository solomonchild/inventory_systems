package org.home.vb.inventorysystem;

import org.home.vb.inventorysystem.features.newsboy.Newsboy;
import org.junit.Assert;
import org.junit.Test;

public class NewsboyTest {

    @Test
    public void normalDistribution() throws InterruptedException {
        Newsboy newsboy = Newsboy.newBuilder()
                .setAcqCost(100)
                .setUnitSellingPrice(200)
                .setSalvageValue(50)
                .setMean(100)
                .setStdDev(20)
                .setOrderCost(20)
                .build();
        Assert.assertEquals(108.6149, newsboy.getOptimalOrderQuantity(), 0.01);
        Assert.assertEquals(8889.099, newsboy.getOptimalExpectedProfit(), 0.01);
        Assert.assertEquals(0.6666, newsboy.getOptimalServiceLevel(), 0.01);
    }

    @Test
    public void normalDistributionWithShortageCost() throws InterruptedException {
        Newsboy newsboy = Newsboy.newBuilder()
                .setAcqCost(100)
                .setUnitSellingPrice(200)
                .setSalvageValue(50)
                .setMean(100)
                .setStdDev(20)
                .setOrderCost(20)
                .setShortageCost(55)
                .build();
        Assert.assertEquals(113.876, newsboy.getOptimalOrderQuantity(), 0.01);
        Assert.assertEquals(8694.311, newsboy.getOptimalExpectedProfit(), 0.01);
        Assert.assertEquals(0.756, newsboy.getOptimalServiceLevel(),  0.01);

    }

}