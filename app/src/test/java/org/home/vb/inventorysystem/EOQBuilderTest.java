package org.home.vb.inventorysystem;

import junit.framework.Assert;

import org.home.vb.inventorysystem.features.eoq.EOQ;
import org.junit.Test;

import java.security.InvalidParameterException;

public class EOQBuilderTest {

    @Test
    public void setValidDemandPerYear() throws Exception {
        int demand = 100;
        EOQ eoq = EOQ.newBuilder().setDemandPerYear(demand).build();
        Assert.assertEquals(demand, eoq.getDemandPerYear());
    }

    @Test(expected=InvalidParameterException.class)
    public void setInvalidDemandPerYear() throws Exception {
        int demand = -1;
        EOQ eoq = EOQ.newBuilder().setDemandPerYear(demand).build();
        Assert.assertEquals(demand, eoq.getDemandPerYear());
    }

    @Test
    public void setValidorderCostPerOrder() throws Exception {
        double orderCost = 20.0;
        EOQ eoq = EOQ.newBuilder().setOrderCostPerOrder(orderCost).build();
        Assert.assertEquals(orderCost, eoq.getOrderCost());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidorderCostPerOrder() throws Exception {
        double orderCost = -1.0;
        EOQ eoq = EOQ.newBuilder().setOrderCostPerOrder(orderCost).build();
    }

    @Test
    public void setValidUnitHoldingCostPerYear() throws Exception {
        double holdingCost = 15.0;
        EOQ eoq = EOQ.newBuilder().setUnitHoldingCostPerYear(holdingCost).build();
        Assert.assertEquals(holdingCost, eoq.getUnitHoldingCostPerYear());
    }

    @Test(expected =InvalidParameterException.class )
    public void setInvalidUnitHoldingCostPerYear() throws Exception {
        double holdingCost = -1.0;
        EOQ eoq = EOQ.newBuilder().setUnitHoldingCostPerYear(holdingCost).build();
    }

    @Test
    public void setValidUnitShortageCostPerYear() throws Exception {
        double shortageCost = 10.0;
        EOQ eoq = EOQ.newBuilder().setUnitShortageCostPerYear(shortageCost).build();
        Assert.assertEquals(shortageCost, eoq.getUnitShortageCostPerYear());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidUnitShortageCostPerYear() throws Exception {
        double shortageCost = -1.0;
        EOQ eoq = EOQ.newBuilder().setUnitShortageCostPerYear(shortageCost).build();
    }

    @Test
    public void setValidReplenishmentRatePerYear() throws Exception {
        double replenishmentRate = 10.0;
        EOQ eoq = EOQ.newBuilder().setReplenishmentRatePerYear(replenishmentRate).build();
        Assert.assertEquals(replenishmentRate, eoq.getReplenishmentRatePerYear());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidReplenishmentRatePerYear() throws Exception {
        double replenishmentRate = -1.0;
        EOQ eoq = EOQ.newBuilder().setReplenishmentRatePerYear(replenishmentRate).build();
    }

    @Test
    public void setValidLeadTime() throws Exception {
        double leadTime = 1.0;
        EOQ eoq = EOQ.newBuilder().setLeadTime(leadTime).build();
        Assert.assertEquals(leadTime, eoq.getLeadTime());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidLeadTimeLarge() throws Exception {
        double leadTime = 1.1;
        EOQ eoq = EOQ.newBuilder().setLeadTime(leadTime).build();
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidLeadTimeNegative() throws Exception {
        double leadTime = -0.1;
        EOQ eoq = EOQ.newBuilder().setLeadTime(leadTime).build();
    }

    @Test
    public void setValidUnitCostWithoutDiscount() throws Exception {
        double unitCost = 100;
        EOQ eoq = EOQ.newBuilder().setUnitCostWithoutDiscount(unitCost).build();
        Assert.assertEquals(unitCost, eoq.getCost());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidUnitCostWithoutDiscount() throws Exception {
        double unitCost = -1;
        EOQ eoq = EOQ.newBuilder().setUnitCostWithoutDiscount(unitCost).build();
    }

    @Test
    public void setValidOptimalQuantity() throws Exception {
        double optimalQuantity = 100;
        EOQ eoq = EOQ.newBuilder().setOptimalQuantity(optimalQuantity).build();
        Assert.assertEquals(optimalQuantity, eoq.getOptimalQuantity());
    }

    @Test(expected = InvalidParameterException.class)
    public void setInvalidOptimalQuantity() throws Exception {
        double optimalQuantity = -1;
        EOQ eoq = EOQ.newBuilder().setOptimalQuantity(optimalQuantity).build();
    }
}
