package org.home.vb.inventorysystem;


import android.support.v4.util.Pair;

import org.home.vb.inventorysystem.features.eoq.EOQ;
import org.home.vb.inventorysystem.features.eoq.EOQGraphHelper;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class EOQGraphHelperTest {
    @Test
    public void getPoints() throws Exception {

        EOQ eoq = EOQ.newBuilder()
                .setDemandPerYear(3600)
                .setOrderCostPerOrder(200)
                .setUnitHoldingCostPerYear(25)
                .setUnitCostWithoutDiscount(100)
                .build();

        List<Pair<Float, Float>> expected = new ArrayList<>(Arrays.asList(
                new Pair<>(0.000000f, 240.000000f),
                new Pair<>(0.066667f, 0.000000f),
                new Pair<>(0.066667f, 240.000000f),
                new Pair<>(0.133333f, 0.000000f),
                new Pair<>(0.133333f, 240.000000f),
                new Pair<>(0.200000f, -0.000027f),
                new Pair<>(0.200000f, 240.000000f),
                new Pair<>(0.266667f, 0.000027f),
                new Pair<>(0.266667f, 240.000000f),
                new Pair<>(0.333333f, 0.000027f),
                new Pair<>(0.333333f, 240.000000f),
                new Pair<>(0.400000f, -0.000080f),
                new Pair<>(0.400000f, 240.000000f),
                new Pair<>(0.466667f, 0.000027f),
                new Pair<>(0.466667f, 240.000000f),
                new Pair<>(0.533333f, 0.000027f),
                new Pair<>(0.533333f, 240.000000f),
                new Pair<>(0.600000f, 0.000027f),
                new Pair<>(0.600000f, 240.000000f),
                new Pair<>(0.666667f, 0.000027f),
                new Pair<>(0.666667f, 240.000000f),
                new Pair<>(0.733333f, 0.000027f),
                new Pair<>(0.733333f, 240.000000f),
                new Pair<>(0.800000f, -0.000188f),
                new Pair<>(0.800000f, 240.000000f),
                new Pair<>(0.866667f, 0.000027f),
                new Pair<>(0.866667f, 240.000000f),
                new Pair<>(0.933333f, 0.000027f),
                new Pair<>(0.933333f, 240.000000f),
                new Pair<>(1.000000f, 0.000241f)
            ));
        List<Pair<Float, Float>> points = EOQGraphHelper.getPoints(eoq);

        Assert.assertEquals(expected.size(), points.size());
        for(int i = 0; i < points.size(); ++i) {
            Assert.assertEquals(expected.get(i).first.floatValue(), points.get(i).first.floatValue(), 0.01);
            Assert.assertEquals(expected.get(i).second.floatValue(), points.get(i).second.floatValue(), 0.01);
        }
    }

    @Test
    public void getPointsForEoqWithDeficit() throws Exception {

        EOQ eoq = EOQ.newBuilder()
                .setDemandPerYear(3600)
                .setOrderCostPerOrder(200)
                .setUnitHoldingCostPerYear(25)
                .setUnitShortageCostPerYear(40)
                .setUnitCostWithoutDiscount(100)
                .build();

        List<Pair<Float, Float>> expected = new ArrayList<>(Arrays.asList(

                new Pair<>(0.000000f, 188.271484f),
                new Pair<>(0.084984f, -117.669678f),
                new Pair<>(0.084984f, 188.271484f),
                new Pair<>(0.169967f, -117.669678f),
                new Pair<>(0.169967f, 188.271484f),
                new Pair<>(0.254951f, -117.669731f),
                new Pair<>(0.254951f, 188.271484f),
                new Pair<>(0.339935f, -117.669624f),
                new Pair<>(0.339935f, 188.271484f),
                new Pair<>(0.424918f, -117.669624f),
                new Pair<>(0.424918f, 188.271484f),
                new Pair<>(0.509902f, -117.669846f),
                new Pair<>(0.509902f, 188.271484f),
                new Pair<>(0.594886f, -117.669624f),
                new Pair<>(0.594886f, 188.271484f),
                new Pair<>(0.679869f, -117.669624f),
                new Pair<>(0.679869f, 188.271484f),
                new Pair<>(0.764853f, -117.669624f),
                new Pair<>(0.764853f, 188.271484f),
                new Pair<>(0.849837f, -117.669624f),
                new Pair<>(0.849837f, 188.271484f),
                new Pair<>(0.934820f, -117.669846f)
        ));
        List<Pair<Float, Float>> points = EOQGraphHelper.getPoints(eoq);

        Assert.assertEquals(expected.size(), points.size());
        for(int i = 0; i < points.size(); ++i) {
            Assert.assertEquals(expected.get(i).first.floatValue(), points.get(i).first.floatValue(), 0.01);
            Assert.assertEquals(expected.get(i).second.floatValue(), points.get(i).second.floatValue(), 0.01);
        }
    }

    @Test
    public void getPointsForEoqWithNonInstantReplenishmentRate() throws Exception {

        EOQ eoq = EOQ.newBuilder()
                .setDemandPerYear(3600)
                .setOrderCostPerOrder(200)
                .setUnitHoldingCostPerYear(25)
                .setReplenishmentRatePerYear(10000)
                .setUnitCostWithoutDiscount(100)
                .build();

        List<Pair<Float, Float>> expected = new ArrayList<>(Arrays.asList(
                new Pair<>(0.000000f, 0.000000f),
                new Pair<>(0.030000f, 192.000000f),
                new Pair<>(0.083333f, 0.000000f),
                new Pair<>(0.113333f, 192.000000f),
                new Pair<>(0.166667f, 0.000000f),
                new Pair<>(0.196667f, 192.000000f),
                new Pair<>(0.250000f, 0.000000f),
                new Pair<>(0.280000f, 192.000000f),
                new Pair<>(0.333333f, 0.000000f),
                new Pair<>(0.363333f, 192.000000f),
                new Pair<>(0.416667f, 0.000000f),
                new Pair<>(0.446667f, 192.000000f),
                new Pair<>(0.500000f, 0.000000f),
                new Pair<>(0.530000f, 192.000000f),
                new Pair<>(0.583333f, 0.000000f),
                new Pair<>(0.613333f, 192.000000f),
                new Pair<>(0.666667f, 0.000000f),
                new Pair<>(0.696667f, 192.000000f),
                new Pair<>(0.750000f, 0.000000f),
                new Pair<>(0.780000f, 192.000000f),
                new Pair<>(0.833333f, 0.000000f),
                new Pair<>(0.863333f, 192.000000f),
                new Pair<>(0.916667f, 0.000000f),
                new Pair<>(0.946667f, 192.000000f),
                new Pair<>(1.000000f, 0.000000f)
        ));
        List<Pair<Float, Float>> points = EOQGraphHelper.getPoints(eoq);

        Assert.assertEquals(expected.size(), points.size());
        for(int i = 0; i < points.size(); ++i) {
            Assert.assertEquals(expected.get(i).first.floatValue(), points.get(i).first.floatValue(), 0.01);
            Assert.assertEquals(expected.get(i).second.floatValue(), points.get(i).second.floatValue(), 0.01);
        }
    }

}