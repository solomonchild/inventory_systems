package org.home.vb.inventorysystem;

import org.home.vb.inventorysystem.features.eoq.EOQ;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class EOQWithDiscountTest {


    //TODO: add test for feasibility and info table
    //TODO: add test when first EOQ is the best one

    @Test
    public void threeDiscountsBasicModel() {
        ArrayList<EOQ.DiscountBreak> discountBreaks = new ArrayList<>();
        discountBreaks.add(new EOQ.DiscountBreak(100, 0.15));
        discountBreaks.add(new EOQ.DiscountBreak(200, 0.25));
        discountBreaks.add(new EOQ.DiscountBreak(2500, 0.27));

        EOQ eoq = EOQ.newBuilder()
                .setDiscountBreaks(discountBreaks)
                .setDemandPerYear(5000)
                .setOrderCostPerOrder(30)
                .setUnitHoldingCostPerYear(15)
                .setUnitCostWithoutDiscount(100)
                .build();
        Assert.assertEquals(200, eoq.getOptimalQuantity(), 0.01);
        Assert.assertEquals(200, eoq.getMaximumInventory(), 0.01);
        //TODO: rename to order interval in a year
        Assert.assertEquals(0.04, eoq.getOrderIntervalPerYear(), 0.01);
        Assert.assertEquals(750, eoq.getTotalOrderingCost(), 0.01);
        Assert.assertEquals(1125, eoq.getTotalHoldingCost(), 0.01);
        Assert.assertEquals(5000*100*0.75, eoq.getTotalMaterialCost(), 0.01);

    }

    @Test
    public void threeDiscountsWithReplenishmentRate() {
        ArrayList<EOQ.DiscountBreak> discountBreaks = new ArrayList<>();
        discountBreaks.add(new EOQ.DiscountBreak(2500, 0.26));
        discountBreaks.add(new EOQ.DiscountBreak(200, 0.25));
        discountBreaks.add(new EOQ.DiscountBreak(100, 0.15));

        EOQ eoq = EOQ.newBuilder()
                .setDiscountBreaks(discountBreaks)
                .setDemandPerYear(5000)
                .setOrderCostPerOrder(15)
                .setUnitHoldingCostPerYear(40)
                .setReplenishmentRatePerYear(7000)
                .setUnitCostWithoutDiscount(100)
                .build();
        Assert.assertEquals(200, eoq.getOptimalQuantity(), 0.01);
        Assert.assertEquals(57.14, eoq.getMaximumInventory(), 0.01);
        //TODO: rename to order interval in a year
        Assert.assertEquals(0.04, eoq.getOrderIntervalPerYear(), 0.01);
        Assert.assertEquals(375, eoq.getTotalOrderingCost(), 0.01);
        Assert.assertEquals(857.14, eoq.getTotalHoldingCost(), 0.01);
        Assert.assertEquals(5000*100*0.75, eoq.getTotalMaterialCost(), 0.01);

    }

    @Test
    public void threeDiscountsWithReplenishmentRateSecondFeasible() {
        ArrayList<EOQ.DiscountBreak> discountBreaks = new ArrayList<>();
        discountBreaks.add(new EOQ.DiscountBreak(100, 0.15));
        discountBreaks.add(new EOQ.DiscountBreak(130, 0.25));
        discountBreaks.add(new EOQ.DiscountBreak(2500, 0.26));

        EOQ eoq = EOQ.newBuilder()
                .setDiscountBreaks(discountBreaks)
                .setDemandPerYear(5000)
                .setOrderCostPerOrder(15)
                .setUnitHoldingCostPerYear(40)
                .setReplenishmentRatePerYear(7000)
                .setUnitCostWithoutDiscount(100)
                .build();
        Assert.assertEquals(132.28, eoq.getOptimalQuantity(), 0.01);
        Assert.assertEquals(37.79, eoq.getMaximumInventory(), 0.01);
        //TODO: rename to order interval in a year
        Assert.assertEquals(0.026, eoq.getOrderIntervalPerYear(), 0.01);
        Assert.assertEquals(566.94, eoq.getTotalOrderingCost(), 0.01);
        Assert.assertEquals(566.94, eoq.getTotalHoldingCost(), 0.01);
        Assert.assertEquals(5000*100*0.75, eoq.getTotalMaterialCost(), 0.01);

    }

    @Test
    public void threeDiscountsWithDeficitSecondFeasible() {
        ArrayList<EOQ.DiscountBreak> discountBreaks = new ArrayList<>();
        discountBreaks.add(new EOQ.DiscountBreak(100, 0.15));
        discountBreaks.add(new EOQ.DiscountBreak(200, 0.25));
        discountBreaks.add(new EOQ.DiscountBreak(2500, 0.26));

        EOQ eoq = EOQ.newBuilder()
                .setDiscountBreaks(discountBreaks)
                .setDemandPerYear(5000)
                .setOrderCostPerOrder(15)
                .setUnitHoldingCostPerYear(40)
                .setUnitShortageCostPerYear(40)
                .setUnitCostWithoutDiscount(100)
                .build();
        Assert.assertEquals(200, eoq.getOptimalQuantity(), 0.01);
        Assert.assertEquals(114.28, eoq.getMaximumInventory(), 0.01);
        Assert.assertEquals(0.04, eoq.getOrderIntervalPerYear(), 0.01);
        Assert.assertEquals(375, eoq.getTotalOrderingCost(), 0.01);
        Assert.assertEquals(979.59, eoq.getTotalHoldingCost(), 0.01);
        Assert.assertEquals(5000*100*0.75, eoq.getTotalMaterialCost(), 0.01);

    }

    @Test
    public void threeDiscountsWithDeficitFirstFeasible() {
        ArrayList<EOQ.DiscountBreak> discountBreaks = new ArrayList<>();
        discountBreaks.add(new EOQ.DiscountBreak(100, 0.15));
        discountBreaks.add(new EOQ.DiscountBreak(200, 0.25));
        discountBreaks.add(new EOQ.DiscountBreak(2500, 0.26));

        EOQ eoq = EOQ.newBuilder()
                .setDiscountBreaks(discountBreaks)
                .setDemandPerYear(5000)
                .setOrderCostPerOrder(15)
                .setUnitHoldingCostPerYear(40)
                .setUnitShortageCostPerYear(40)
                .setUnitCostWithoutDiscount(100)
                .build();
        Assert.assertEquals(200, eoq.getOptimalQuantity(), 0.01);
        Assert.assertEquals(114.28, eoq.getMaximumInventory(), 0.01);
        Assert.assertEquals(85.71, eoq.getMaximumBackorder(), 0.01);
        //TODO: rename to order interval in a year
        Assert.assertEquals(0.04, eoq.getOrderIntervalPerYear(), 0.01);
        Assert.assertEquals(-85.71, eoq.getROP(), 0.01);

        Assert.assertEquals(375, eoq.getTotalOrderingCost(), 0.01);
        Assert.assertEquals(979.59, eoq.getTotalHoldingCost(), 0.01);
        Assert.assertEquals(734.69, eoq.getTotalShortageCost(), 0.01);
        Assert.assertEquals(375000, eoq.getTotalMaterialCost(), 0.01);

    }

}
