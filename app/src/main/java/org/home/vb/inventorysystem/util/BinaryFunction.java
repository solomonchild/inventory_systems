package org.home.vb.inventorysystem.util;

public interface BinaryFunction<P1, P2, Return> {
    Return apply(P1 param1, P2 param2);
}
