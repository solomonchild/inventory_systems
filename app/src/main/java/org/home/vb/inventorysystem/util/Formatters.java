package org.home.vb.inventorysystem.util;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.text.ParseException;

public class Formatters {

    public static Double formatDoubleToThreeDecimals(Double value) {
        DecimalFormat df = new DecimalFormat("#.###");
        String formatted = df.format(value);
        try {
            return df.parse(formatted).doubleValue();
        } catch (ParseException e) {
            return -1.0;
        }
    }

    public static class ValueFormatter implements IValueFormatter {

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            DecimalFormat format = new DecimalFormat("########0.0");
            return format.format(value);
        }
    }

    public static class AxisFormatter implements IAxisValueFormatter {

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            DecimalFormat format = new DecimalFormat("########0.0");
            return format.format(value);
        }
    }
}
