package org.home.vb.inventorysystem.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.home.vb.inventorysystem.R;
import org.home.vb.inventorysystem.features.eoq.EOQActivity;
import org.home.vb.inventorysystem.features.newsboy.NewsboyActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button eoqButton = (Button) findViewById(R.id.button_eoq);
        eoqButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EOQActivity.class);
                startActivity(intent);
            }
        });

        Button newsboyButton = (Button) findViewById(R.id.button_newsboy);
        newsboyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewsboyActivity.class);
                startActivity(intent);
            }
        });
    }
}
