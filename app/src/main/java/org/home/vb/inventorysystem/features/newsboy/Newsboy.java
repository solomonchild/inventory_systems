package org.home.vb.inventorysystem.features.newsboy;

import android.support.v4.util.Pair;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.home.vb.inventorysystem.util.BinaryFunction;
import org.home.vb.inventorysystem.util.ParallelFor;
import org.home.vb.inventorysystem.util.UnaryFunction;

import java.io.Serializable;
import java.security.InvalidParameterException;

public class Newsboy implements Serializable{

    private double mMean = 0;
    private double mStdDev = 0;
    private double mOrderCost = 0;
    private double mAcqCost = 0;
    private double mUnitSellingPrice = 0;
    private double mShortageCost = 0;
    private double mSalvageValue = 0;

    public double getSalvageValue() {
        return mSalvageValue;
    }

    public double getShortageCost() {
        return mShortageCost;
    }

    public double getOptimalOrderQuantity() {
        return mOptimalOrderQuantity;
    }

    public double getOptimalServiceLevel() {
        return mOptimalServiceLevel;
    }

    public double getOptimalExpectedProfit() {
        return mOptimalExpectedProfit;
    }

    private double mOptimalOrderQuantity = Double.NaN;
    private double mOptimalServiceLevel = Double.NaN;
    private double mOptimalExpectedProfit = Double.NaN;

    private Newsboy () {}

    private NormalDistribution mNorm;
    private Pair<Double, Double> execute() throws InterruptedException {

        UnaryFunction<Integer, Pair<Double, Double>> f = new UnaryFunction<Integer, Pair<Double, Double>>() {
            @Override
            public Pair<Double, Double> apply(Integer param) {
                double density = mNorm.density(param);
                return new Pair<>(density * Math.min(mOptimalOrderQuantity, param), Math.max(param - mOptimalOrderQuantity, 0) * density);
            }
        };
        BinaryFunction<Pair<Double, Double>, Pair<Double, Double>, Pair<Double, Double>> f2 = new BinaryFunction<Pair<Double, Double>, Pair<Double, Double>, Pair<Double, Double>>() {
            @Override
            public Pair<Double, Double> apply(Pair<Double, Double> t1, Pair<Double, Double> t2) {
                if(t2 == null) {
                    t2 = new Pair<>(0.0, 0.0);
                }
                return new Pair<>(t1.first + t2.first, t1.second + t2.second);
            }
        };
        if(mOptimalOrderQuantity < 0) {
            throw new InvalidParameterException("Optimal order quantity is negative. Cannot calculate");
        }
        ParallelFor parallelFor = new ParallelFor((int) (3*mOptimalOrderQuantity));
        Pair<Double, Double> result = (Pair<Double, Double>) parallelFor.process(f, f2);
        return result;

    }

    public Newsboy clone() {
        try {
            return builderFromThis().build();
        } catch (InterruptedException e) {
            throw new InvalidParameterException(e.getMessage());
        }
    }

    private void calculate() throws InterruptedException {
        mNorm = new NormalDistribution(mMean, mStdDev);
        double profit_1 = mUnitSellingPrice - mAcqCost + mShortageCost;
        double loss_1 = mAcqCost - mSalvageValue;
        if(Double.isNaN(mOptimalServiceLevel)) {
            mOptimalServiceLevel = profit_1 / (profit_1 + loss_1);
        }
        if(Double.isNaN(mOptimalOrderQuantity)) {
            mOptimalOrderQuantity = mNorm.inverseCumulativeProbability(mOptimalServiceLevel);
        }

        double estimatedSold = 0.0f;
        double estimatedLost = 0.0f;
        if(!Double.isInfinite(mOptimalOrderQuantity)) {
            Pair<Double, Double> d = execute();
            estimatedSold = d.first;
            estimatedLost = d.second;
            mOptimalExpectedProfit = (profit_1 + loss_1 - mShortageCost) * estimatedSold - loss_1 * mOptimalOrderQuantity - mShortageCost * estimatedLost;
            mOptimalExpectedProfit -= mOrderCost;
        }
        else {
            mOptimalExpectedProfit = 0;
        }
    }
    static public Builder newBuilder() {
        return new Newsboy().new Builder();
    }
    public Builder builderFromThis() {
        return new Newsboy().new Builder(this);
    }

    public double getMean() {
        return mMean;
    }

    public double getStdDev() {
        return mStdDev;
    }

    public double getOrderCost() {
        return mOrderCost;
    }

    public double getAcqCost() {
        return mAcqCost;
    }

    public Double getUnitSellingPrice() {
        return mUnitSellingPrice;
    }

    public class Builder {

        public Builder() { }

        public Builder(Newsboy old) {

            Newsboy.this.mMean = old.mMean;
            Newsboy.this.mStdDev = old.mStdDev;
            Newsboy.this.mOrderCost = old.mOrderCost;
            Newsboy.this.mAcqCost = old.mAcqCost;
            Newsboy.this.mUnitSellingPrice = old.mUnitSellingPrice;
            Newsboy.this.mShortageCost = old.mShortageCost;
            Newsboy.this.mSalvageValue = old.mSalvageValue;


            Newsboy.this.mOptimalOrderQuantity = old.mOptimalOrderQuantity;
            Newsboy.this.mOptimalServiceLevel = old.mOptimalServiceLevel;
            Newsboy.this.mOptimalExpectedProfit = old.mOptimalExpectedProfit;
        }

        public Builder setMean(double mean) {
            if(mean < 0) {
                throw new InvalidParameterException("Mean cannot be negative");
            }
            Newsboy.this.mMean = mean;
            return this;
        }

        public Builder setStdDev(double stdDev) {
            if(stdDev <= 0 ) {
                throw new InvalidParameterException("Standard deviation must be strictly positive");
            }
            Newsboy.this.mStdDev = stdDev;
            return this;
        }

        public Builder setOrderCost(double orderCost) {
            if(orderCost < 0) {
                throw new InvalidParameterException("Order cost cannot be negative");
            }
            Newsboy.this.mOrderCost = orderCost;
            return this;
        }

        public Builder setAcqCost(double acqCost) {
            if(acqCost < 0) {
                throw new InvalidParameterException("Acquisition cost cannot be negative");
            }
            Newsboy.this.mAcqCost = acqCost;
            return this;
        }

        public Builder setUnitSellingPrice(double unitSellingPrice) {
            if(unitSellingPrice < 0) {
                throw new InvalidParameterException("Unit selling price cannot be negative");
            }
            Newsboy.this.mUnitSellingPrice = unitSellingPrice;
            return this;
        }

        public Builder setShortageCost(double shortageCost) {
            if(shortageCost < 0) {
                throw new InvalidParameterException("Shortage cost cannot be negative");
            }
            Newsboy.this.mShortageCost = shortageCost;
            return this;
        }

        public Builder setSalvageValue(double salvageValue) {
            if(salvageValue < 0) {
                throw new InvalidParameterException("Salvage value cannot be negative");
            }
            Newsboy.this.mSalvageValue = salvageValue;
            return this;
        }

        public Builder setOptimalOrderQuantity(double orderQuantity) {
            if(orderQuantity < 0){
                throw new InvalidParameterException("Order quantity cannot be negative");
            }
            Newsboy.this.mOptimalOrderQuantity = orderQuantity;
            return this;
        }

        public Builder setOptimalServiceLevel(double serviceLevel) {
            if(serviceLevel < 0 || serviceLevel > 1) {
                throw new InvalidParameterException("Service level must be in range 0-1");
            }
            Newsboy.this.mOptimalServiceLevel = serviceLevel;
            return this;
        }

        public Newsboy build() throws InterruptedException {
            Newsboy.this.calculate();
            return Newsboy.this;
        }


    }

}
