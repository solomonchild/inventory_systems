package org.home.vb.inventorysystem.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;

import org.home.vb.inventorysystem.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public class SdkHelpers {
    public static void showErrorNotification(Context ctx, String message) {
        new AlertDialog.Builder(ctx).setTitle("Error").setPositiveButton("Ok", null).setMessage(message).create().show();
    }

    static private File setupBaseDir(Context ctx) throws IOException {
        File dirs = new File(ctx.getFilesDir() + "/files/");
        //File dirs = new File(Environment.getExternalStorageDirectory().getCanonicalPath() + "/" + APP_DIR);
        if(!Environment.getExternalStorageDirectory().canWrite()) {
            new IOException("Cannot write to external storage");
        }
        if(!dirs.exists()) {
            if(!dirs.mkdirs()) {
                new IOException("Cannot build a new directory in external storage");
            }
        }
        return dirs;
    }

    public static Uri saveBitmapToFile(Context ctx, Bitmap bmp, String filename) throws IOException {
        File dirs = setupBaseDir(ctx);
        OutputStream os;

        File file = new File(dirs, filename + ".png");
        file.createNewFile();
        file.deleteOnExit();

        os = new FileOutputStream(file);
        bmp.compress(Bitmap.CompressFormat.PNG, 100, os);
        os.flush();
        os.close();
        return FileProvider.getUriForFile(ctx,ctx.getString(R.string.authority_content_provider), file);
    }

    public static Uri saveMapToCsv(Context ctx, Map<String, String> values, String filename) throws IOException {
        File dirs = setupBaseDir(ctx);
        OutputStream os;

        File file = new File(dirs, filename + ".csv");
        file.createNewFile();
        file.deleteOnExit();

        os = new FileOutputStream(file);
        for(Map.Entry<String, String> entry : values.entrySet()) {
            String result = entry.getKey() + ", " + entry.getValue() + System.lineSeparator();
            os.write(result.getBytes());
        }
        return FileProvider.getUriForFile(ctx,ctx.getString(R.string.authority_content_provider), file);
    }

    public static Double getDoubleIfNotNull(EditText et) {

        if(et.getText().length() == 0)
        {
            return null;
        }
        return Double.parseDouble(et.getText().toString());
    }
}
