package org.home.vb.inventorysystem.features.eoq;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.home.vb.inventorysystem.R;
import org.home.vb.inventorysystem.databinding.ActivityEoqResultBinding;
import org.home.vb.inventorysystem.features.export.ExportUtility;
import org.home.vb.inventorysystem.util.BinaryFunction;
import org.home.vb.inventorysystem.util.SdkHelpers;
import org.home.vb.inventorysystem.util.UnaryFunction;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class EOQResultActivity extends AppCompatActivity {

    static String EOQ_EXTRA_ID = "EOQ";

    public static Intent getIntent(Context context, EOQ eoq) {
        Intent intent = new Intent(context, EOQResultActivity.class);
        intent.putExtra(EOQ_EXTRA_ID, eoq);
        return intent;
    }

    private EOQ mEoq;
    private EOQ mOriginalEoq;
    private LineChart mQuantityGraph;
    private LineChart mTCGraph;
    AppCompatSpinner mPropertySelectorSpinner;
    SeekBar mPropertySelectorSeekBar;
    TextView mPropertySelectorTextView;
    ActivityEoqResultBinding mBinding;
    Button mResetButton;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.export_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_export) {
            ArrayList<Uri> graphLocations = new ArrayList<>();
            ArrayList<Uri> dataLocations = new ArrayList<>();

            HashMap<String, String> values = new HashMap<>();
            values.put("Maximum Backorder", String.valueOf(mEoq.getMaximumBackorder()));
            values.put("Maximum Inventory", String.valueOf(mEoq.getMaximumInventory()));
            values.put("Optimal Quantity", String.valueOf(mEoq.getOptimalQuantity()));
            values.put("Order interval per year", String.valueOf(mEoq.getOrderIntervalPerYear()));
            values.put("Reorder point", String.valueOf(mEoq.getROP()));
            values.put("Total Costs", String.valueOf(mEoq.getTC()));
            values.put("Total Holding Costs", String.valueOf(mEoq.getTotalHoldingCost()));
            values.put("Total Material Costs", String.valueOf(mEoq.getTotalMaterialCost()));
            values.put("Total Shortage Costs", String.valueOf(mEoq.getTotalShortageCost()));
            try {
                graphLocations.add(SdkHelpers.saveBitmapToFile(this, mQuantityGraph.getChartBitmap(), "quantity_graph"));
                graphLocations.add(SdkHelpers.saveBitmapToFile(this, mTCGraph.getChartBitmap(), "TC_graph"));
                dataLocations.add(SdkHelpers.saveMapToCsv(this, values, "values"));
            } catch (IOException e) {
                SdkHelpers.showErrorNotification(this, e.getMessage());
                return super.onOptionsItemSelected(item);
            }

            ExportUtility.export(dataLocations, graphLocations, this);
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupTotalCostGraph() {
        try {

            mTCGraph = (LineChart) findViewById(R.id.graph);
            double percentOfAfter = 0.7;
            int numberOfPoints = 20;
            double maxY = 0.0f;
            double step = (mEoq.getOptimalQuantity() * (1 + percentOfAfter)) / numberOfPoints;
            List<Entry> totalOrderingCostEntrys = new ArrayList<>();
            List<Entry> totalHoldingCostEntrys = new ArrayList<>();
            List<Entry> TCEntrys = new ArrayList<>();
            List<Entry> totalShortageCostEntrys = null;
            if (mEoq.getTotalShortageCost() != 0) {
                totalShortageCostEntrys = new ArrayList<>();
            }

            if (!Double.isNaN(mEoq.getOptimalQuantity())) {
                for (int i = 0; i < numberOfPoints; ++i) {

                    EOQ.Builder newBuilder = mEoq.builderFromThis();
                    newBuilder.setOptimalQuantity(step * i);
                    EOQ newEOQ = newBuilder.build();
                    if (mEoq.getTotalShortageCost() != 0) {
                        totalShortageCostEntrys.add(new Entry((float) newEOQ.getOptimalQuantity(), (float) newEOQ.getTotalShortageCost()));
                    }
                    totalHoldingCostEntrys.add(new Entry((float) newEOQ.getOptimalQuantity(), (float) newEOQ.getTotalHoldingCost()));
                    if (i != 0) {
                        totalOrderingCostEntrys.add(new Entry((float) newEOQ.getOptimalQuantity(), (float) newEOQ.getTotalOrderingCost()));
                        double total = newEOQ.getTotalHoldingCost() + newEOQ.getTotalOrderingCost();
                        if (newEOQ.getTotalShortageCost() != 0) {
                            total += newEOQ.getTotalShortageCost();
                        }
                        TCEntrys.add(new Entry((float) newEOQ.getOptimalQuantity(), (float) total));
                        maxY = Math.max(Math.max(maxY, newEOQ.getTotalOrderingCost()), newEOQ.getTotalHoldingCost());
                    }
                }
                LineDataSet orderingCostSeries = new LineDataSet(totalOrderingCostEntrys, "Ordering Cost");
                orderingCostSeries.setColor(Color.MAGENTA);

                LineDataSet holdingCostSeries = new LineDataSet(totalHoldingCostEntrys, "Holding Cost");
                holdingCostSeries.setColor(Color.GREEN);

                LineDataSet totalCostSeries = new LineDataSet(TCEntrys, "Total Cost");
                totalCostSeries.setColor(Color.RED);
                LineData lineData = new LineData();
                if (totalShortageCostEntrys != null) {
                    LineDataSet totalShortageSeries = new LineDataSet(totalShortageCostEntrys, "Total Shortage Cost");
                    totalShortageSeries.setColor(Color.BLACK);
                    lineData.addDataSet(totalShortageSeries);
                }


                lineData.addDataSet(orderingCostSeries);
                lineData.addDataSet(holdingCostSeries);
                lineData.addDataSet(totalCostSeries);
                lineData.setDrawValues(false);

                LimitLine ll = new LimitLine((float) mEoq.getOptimalQuantity(), "EOQ");
                ll.setTextSize(8);

                mTCGraph.getXAxis().removeAllLimitLines();
                mTCGraph.getXAxis().addLimitLine(ll);
                mTCGraph.setData(lineData);
                mTCGraph.getLegend().setWordWrapEnabled(true);
                mTCGraph.getXAxis().setAxisMaximum((float) (mEoq.getOptimalQuantity() * (1 + percentOfAfter)));
                mTCGraph.getXAxis().setAxisMinimum(0);

                mTCGraph.getAxisRight().setDrawLabels(false);
                mTCGraph.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                mTCGraph.getXAxis().setDrawAxisLine(true);
                mTCGraph.getXAxis().setDrawGridLines(true);
                mTCGraph.getXAxis().setGranularity(0.01f);
                mTCGraph.getXAxis().setGranularityEnabled(true);
                Description desc = new Description();
                desc.setText("Q");
                mTCGraph.setDescription(desc);
                mTCGraph.invalidate();
            }
        } catch (InvalidParameterException ex) {
            new AlertDialog.Builder(EOQResultActivity.this).setTitle("Error").setMessage(ex.getMessage()).setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).create().show();
        }

    }

    private void setupGraph() {
        setupQuantityGraph();
        if (!mEoq.hasDiscounts())
            setupTotalCostGraph();
        else
            setupDiscountGraph();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupGraph();
        setupSpinner();
    }


    private void onEOQChanged() {
        mBinding.setEoq(mEoq);
        mBinding.executePendingBindings();
        setupGraph();
    }
    private void setupSeekBar(final String item, UnaryFunction<EOQ, Double> getter, final BinaryFunction<Integer, EOQ.Builder, EOQ.Builder> setter) {

        double fieldValue = getter.apply(mEoq);
        double maxValue = fieldValue == 0 ? 9999 : fieldValue * 2;
        mPropertySelectorTextView.setText(String.valueOf(fieldValue));
        mPropertySelectorSeekBar.setOnSeekBarChangeListener(null);
        mPropertySelectorSeekBar.setProgress(0);
        mPropertySelectorSeekBar.setMax((int) maxValue);
        mPropertySelectorSeekBar.setProgress((int) fieldValue);
        mPropertySelectorSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mPropertySelectorTextView.setText(String.valueOf(progress));
                try {
                    EOQ.Builder builder = mEoq.builderFromThisWithDiscounts();
                    mEoq = setter.apply(progress, builder).build();
                    onEOQChanged();
                }
                catch (Exception e) {
                    SdkHelpers.showErrorNotification(EOQResultActivity.this, e.getMessage());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setupSpinner() {

        final String[] items = {
                getResources().getString(R.string.demand_per_year),
                getResources().getString(R.string.order_cost),
                getResources().getString(R.string.holding_cost_per_year),
                getResources().getString(R.string.shortage_cost_per_year),
                getResources().getString(R.string.replenishment_rate_per_year),
                getResources().getString(R.string.lead_time),
                getResources().getString(R.string.acquisition_cost),
        };
        final BinaryFunction<Integer, EOQ.Builder, EOQ.Builder>[] binaryFunctions = new BinaryFunction[] {
                new BinaryFunction<Integer, EOQ.Builder, EOQ.Builder>() {
                    @Override
                    public EOQ.Builder apply(Integer t1, EOQ.Builder t2) {
                        t2.setDemandPerYear(t1);
                        return t2;
                    }
                },
                new BinaryFunction<Integer, EOQ.Builder, EOQ.Builder>() {
                    @Override
                    public EOQ.Builder apply(Integer t1, EOQ.Builder t2) {
                        t2.setOrderCostPerOrder(t1);
                        return t2;
                    }
                },
                new BinaryFunction<Integer, EOQ.Builder, EOQ.Builder>() {
                    @Override
                    public EOQ.Builder apply(Integer t1, EOQ.Builder t2) {
                        t2.setUnitHoldingCostPerYear(t1);
                        return t2;
                    }
                },
                new BinaryFunction<Integer, EOQ.Builder, EOQ.Builder>() {
                    @Override
                    public EOQ.Builder apply(Integer t1, EOQ.Builder t2) {
                        t2.setUnitShortageCostPerYear(t1);
                        return t2;
                    }
                },
                new BinaryFunction<Integer, EOQ.Builder, EOQ.Builder>() {
                    @Override
                    public EOQ.Builder apply(Integer t1, EOQ.Builder t2) {
                        t2.setReplenishmentRatePerYear(t1);
                        return t2;
                    }
                },
                new BinaryFunction<Integer, EOQ.Builder, EOQ.Builder>() {
                    @Override
                    public EOQ.Builder apply(Integer t1, EOQ.Builder t2) {
                        t2.setLeadTime(t1);
                        return t2;
                    }
                },
                new BinaryFunction<Integer, EOQ.Builder, EOQ.Builder>() {
                    @Override
                    public EOQ.Builder apply(Integer t1, EOQ.Builder t2) {
                        t2.setUnitCostWithoutDiscount(t1);
                        return t2;
                    }
                },



        };
        final UnaryFunction<EOQ, Double>[] unaryFunctions = new UnaryFunction[] {
                new UnaryFunction<EOQ, Double>() {
                    @Override
                    public Double apply(EOQ param) {
                        return Double.valueOf(param.getDemandPerYear());
                    }
                },
                new UnaryFunction<EOQ, Double>() {
                    @Override
                    public Double apply(EOQ param) {
                        return param.getOrderCost();
                    }
                },
                new UnaryFunction<EOQ, Double>() {
                    @Override
                    public Double apply(EOQ param) {
                        return param.getUnitHoldingCostPerYear();
                    }
                },
                new UnaryFunction<EOQ, Double>() {
                    @Override
                    public Double apply(EOQ param) {
                        return param.getUnitShortageCostPerYear();
                    }
                },

                new UnaryFunction<EOQ, Double>() {
                    @Override
                    public Double apply(EOQ param) {
                        return param.getReplenishmentRatePerYear();
                    }
                },
                new UnaryFunction<EOQ, Double>() {
                    @Override
                    public Double apply(EOQ param) {
                        return param.getLeadTime();
                    }
                },
                new UnaryFunction<EOQ, Double>() {
                    @Override
                    public Double apply(EOQ param) {
                        return param.getCost();
                    }
                },


        };
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, items);
        mPropertySelectorSpinner.setAdapter(adapter);
        mPropertySelectorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position >= 0 && position < items.length){
                    setupSeekBar(items[position], unaryFunctions[position], binaryFunctions[position]);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eoq_result);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mEoq = (EOQ) getIntent().getSerializableExtra(EOQ_EXTRA_ID);
        mOriginalEoq = mEoq.clone();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_eoq_result);
        mBinding.setEoq(mEoq);
        mPropertySelectorSeekBar = (SeekBar) findViewById(R.id.seek_bar_property_selector_eoq);
        mPropertySelectorSpinner = (AppCompatSpinner) findViewById(R.id.spinner_property_selector_eoq);
        mPropertySelectorTextView = (TextView) findViewById(R.id.text_view_property_selector_eoq);
        mResetButton = (Button) findViewById(R.id.button_reset_eoq);
        mResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEoq = mOriginalEoq.clone();
                onEOQChanged();

                mPropertySelectorSpinner.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        int selectedPosition = mPropertySelectorSpinner.getSelectedItemPosition();
                        int next = (selectedPosition +1) % mPropertySelectorSpinner.getAdapter().getCount();
                        mPropertySelectorSpinner.setSelection(next, true);
                        mPropertySelectorSpinner.setSelection(selectedPosition, true);
                    }
                }, 100);
            }
        });
    }

    private void setupDiscountGraph() {
        mTCGraph = (LineChart) findViewById(R.id.graph);
        int numberOfPoints = 10;
        double maxY = 0.0f;
        double start = 0;
        LineData lineData = new LineData();
        double cost = mEoq.getCost();
        ArrayList<EOQ.DiscountBreak> discountBreaks = (ArrayList<EOQ.DiscountBreak>) mEoq.getDiscounts().clone();
        discountBreaks.add(new EOQ.DiscountBreak((int) (discountBreaks.get(discountBreaks.size() - 1).getUnits() * 1.25), 0.25));
        int startIndex = 1;
        for (EOQ.DiscountBreak discountBreak : discountBreaks) {
            double disc = discountBreak.getUnits();
            double step = (disc - start) / numberOfPoints;
            List<Entry> TCEntrys = new ArrayList<>();

            if (!Double.isNaN(mEoq.getOptimalQuantity())) {
                for (int i = startIndex; i <= numberOfPoints; ++i) {

                    EOQ.Builder newBuilder = mEoq.builderFromThis();
                    newBuilder.setOptimalQuantity(start + step * i)
                    //TODO: remove "without discount"
                    .setUnitCostWithoutDiscount(cost);

                    EOQ newEOQ = newBuilder.build();
                    TCEntrys.add(new Entry((float) newEOQ.getOptimalQuantity(), (float) newEOQ.getTC()));
                    maxY = Math.max(Math.max(maxY, newEOQ.getTotalOrderingCost()), newEOQ.getTotalHoldingCost());
                }
                if(startIndex > 0) {
                    startIndex --;
                }

                LineDataSet totalCostSeries = new LineDataSet(TCEntrys, "Total Cost");
                totalCostSeries.setColor(Color.RED);
                lineData.addDataSet(totalCostSeries);
                lineData.setDrawValues(false);
            }

            start = disc;
            //check for discount being < 1
            cost = ((100 - discountBreak.getDiscount())/100.0) * mEoq.getCost();
        }

        mTCGraph.getXAxis().removeAllLimitLines();
        for(EOQ.DiscountBreak b : mEoq.getDiscounts()) {
            LimitLine ll = new LimitLine((float)b.getUnits(), "");
            mTCGraph.getXAxis().addLimitLine(ll);
        }


        mTCGraph.setData(lineData);
        mTCGraph.getLegend().setWordWrapEnabled(true);
        double max = Math.max(mEoq.getOptimalQuantity(), mEoq.getDiscounts().get(mEoq.getDiscounts().size() - 1).getUnits());
        mTCGraph.getXAxis().setAxisMaximum((float) (mEoq.getOptimalQuantity() * (1 + 0.25)));
        mTCGraph.getXAxis().setAxisMinimum(0);
        Description desc = new Description();
        desc.setText("Q");
        mTCGraph.setDescription(desc);

        mTCGraph.getAxisRight().setDrawLabels(false);
        mTCGraph.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mTCGraph.getXAxis().setDrawAxisLine(true);
        mTCGraph.getXAxis().setDrawGridLines(true);
        mTCGraph.getXAxis().setGranularity(0.01f);
        mTCGraph.getXAxis().setGranularityEnabled(true);
        mTCGraph.invalidate();
    }

    private void setupQuantityGraph() {
        mQuantityGraph = (LineChart) findViewById(R.id.graph_eoq_quantity_warehouse);
        List<Entry> series = new ArrayList<>();
        List<Pair<Float, Float>> points = EOQGraphHelper.getPoints(mEoq);
        for(Pair<Float, Float> p : points) {
            series.add(new Entry(p.first.floatValue(), p.second.floatValue()));
        }
        LineDataSet quantitySet = new LineDataSet(series, "q=f(t)");
        quantitySet.setColor(Color.RED);

        LineData data = new LineData();
        data.addDataSet(quantitySet);
        mQuantityGraph.setData(data);
        data.setDrawValues(false);
        mQuantityGraph.getXAxis().setAxisMaximum(1.1f);
        Description descr = new Description();
        descr.setText("Years");

        mQuantityGraph.getAxisLeft().removeAllLimitLines();
        LimitLine ropLimitLine = new LimitLine((float)mEoq.getROP(), "Reorder Point");
        ropLimitLine.enableDashedLine(5,5,0);
        ropLimitLine.setTextSize(8);
        ropLimitLine.setLineColor(Color.BLACK);
        mQuantityGraph.getAxisLeft().addLimitLine(ropLimitLine);

        LimitLine maxInventoryLimitLine = new LimitLine((float)mEoq.getMaximumInventory(), "Maximum Inventory");
        maxInventoryLimitLine.setTextSize(8);
        maxInventoryLimitLine.setLineColor(Color.BLACK);
        maxInventoryLimitLine.enableDashedLine(5,5,0);
        mQuantityGraph.getAxisLeft().addLimitLine(maxInventoryLimitLine);

        mQuantityGraph.setDescription(descr);
        mQuantityGraph.getAxisRight().setDrawLabels(false);
        mQuantityGraph.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mQuantityGraph.getXAxis().setDrawAxisLine(true);
        mQuantityGraph.getXAxis().setDrawGridLines(true);
        mQuantityGraph.getXAxis().setGranularity(0.01f);
        mQuantityGraph.getXAxis().setGranularityEnabled(true);
        mQuantityGraph.invalidate();
    }
}
