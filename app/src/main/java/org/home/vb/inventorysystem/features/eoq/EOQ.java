package org.home.vb.inventorysystem.features.eoq;


import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public final class EOQ implements Serializable {


    public int getDemandPerYear() {
        return mInParams.mDemandPerYear;
    }

    public double getOrderCost() {
        return mInParams.mOrderCost;
    }

    public double getUnitHoldingCostPerYear() {
        return mInParams.mUnitHoldingCostPerYear;
    }

    public double getUnitShortageCostPerYear() {
        return mInParams.mUnitShortageCostPerYear;
    }

    public double getReplenishmentRatePerYear() {
        return mInParams.mReplenishmentRatePerYear;
    }

    public double getLeadTime() {
        return mInParams.mLeadTime;
    }

    public double getCost() {
        return mInParams.mCost;
    }

    public EOQ clone() { return builderFromThisWithDiscounts().build(); }
    public class InParameters implements Serializable {

        int    mDemandPerYear = 0;
        double mOrderCost = 0.0f;
        double mUnitHoldingCostPerYear = 0.0f;
        double mUnitShortageCostPerYear = 0.0f;
        double mReplenishmentRatePerYear = 0.0f;
        double mLeadTime = 0.0f;
        double mCost = 0.0f;


        double mOpportunityCost = 0.0f;

        public InParameters() {}

        public InParameters(InParameters old) {
            mDemandPerYear = old.mDemandPerYear;
            mOrderCost = old.mOrderCost;
            mUnitHoldingCostPerYear = old.mUnitHoldingCostPerYear;
            mUnitShortageCostPerYear = old.mUnitShortageCostPerYear;
            mReplenishmentRatePerYear = old.mReplenishmentRatePerYear;
            mLeadTime = old.mLeadTime;
            mCost = old.mCost;
            mOpportunityCost = old.mOpportunityCost;
        }
    }


    private OutParameters mOutParams;
    private InParameters mInParams;

    private ArrayList<DiscountBreak> mDiscountBreaks;


    public double getTotalMaterialCost() {
        return mOutParams.mTotalMaterialCost;
    }

    public double getMaximumBackorder() {
        return mOutParams.mMaximumBackorder;
    }

    public double getOrderIntervalPerYear() {
        return mOutParams.mOrderIntervalPerYear;
    }

    public double getROP() {
        return mOutParams.mROP;
    }

    public double getTotalOrderingCost() {
        return mOutParams.mTotalOrderingCost;
    }

    public double getTotalHoldingCost() {
        return mOutParams.mTotalHoldingCost;
    }

    public double getTotalShortageCost() {
        return mOutParams.mTotalShortageCost;
    }

    public double getOptimalQuantity() {
        return mOutParams.mOptimalQuantity;
    }

    public double getMaximumInventory() {
        return mOutParams.mMaximumInventory;
    }

    public double getTC() {
        return mOutParams.TC;
    }

    private EOQ() {
    }


    public boolean isDeficitAllowed() {
        return !Double.isInfinite(mInParams.mUnitShortageCostPerYear);
    }

    public boolean isReplenishmentInstantaneous() {
        return Double.isInfinite(mInParams.mReplenishmentRatePerYear);
    }


    public boolean hasDiscounts() {
        return mDiscountBreaks != null && !mDiscountBreaks.isEmpty();
    }
    public ArrayList<DiscountBreak> getDiscounts() { return mDiscountBreaks; }

    protected OutParameters calculate(InParameters params) {
        OutParameters pack;
        if(isDeficitAllowed() && !isReplenishmentInstantaneous()) {
            pack = OutParameters.calculateWithDeficitAndReplenishmentRate(params);
        }
        else if(isReplenishmentInstantaneous() && isDeficitAllowed()) {
            pack = OutParameters.calculateWithDeficitAndInstanteneousReplenishment(params);
        }
        else if(!isReplenishmentInstantaneous() && !isDeficitAllowed()) {
            pack = OutParameters.calculateWithNoDeficitAndReplenishmentRate(params);
        }
        else {
            pack = OutParameters.calculateWithNoDeficitAndInstanteneousReplenishment(params);
        }
        return pack;
    }

    protected OutParameters calculate(InParameters params, double optimalQuantity) {
        OutParameters pack;
        if(isDeficitAllowed() && !isReplenishmentInstantaneous()) {
            pack = OutParameters.calculateWithDeficitAndReplenishmentRate(params, optimalQuantity);
        }
        else if(isReplenishmentInstantaneous() && isDeficitAllowed()) {
            pack = OutParameters.calculateWithDeficitAndInstanteneousReplenishment(params, optimalQuantity);
        }
        else if(!isReplenishmentInstantaneous() && !isDeficitAllowed()) {
            pack = OutParameters.calculateWithNoDeficitAndReplenishmentRate(params, optimalQuantity);
        }
        else {
            pack = OutParameters.calculateWithNoDeficitAndInstanteneousReplenishment(params, optimalQuantity);
        }
        return pack;
    }

    protected void calculateEOQ() {
        if(Double.isNaN(mOutParams.mOptimalQuantity)) {

            mOutParams = calculate(mInParams);
        }
        else {
            mOutParams = calculate(mInParams, mOutParams.mOptimalQuantity);
        }

        if(hasDiscounts()) {
            Collections.sort(mDiscountBreaks, new Comparator<DiscountBreak>() {
                @Override
                public int compare(DiscountBreak o1, DiscountBreak o2) {
                    return o1.getUnits() - o2.getUnits();
                }
            });
            double holdingFraction = mInParams.mUnitHoldingCostPerYear / mInParams.mCost;
            double TC = mOutParams.TC;

            for (DiscountBreak discountBreak : mDiscountBreaks) {
                    double cost = (1 - discountBreak.getDiscount()) * mInParams.mCost;

                    double unitHoldingCost = cost * holdingFraction;
                    InParameters params = new InParameters(mInParams);
                    params.mCost = cost;
                    params.mUnitHoldingCostPerYear = unitHoldingCost;
                    OutParameters pack = calculate(params);

                    //feasible
                    if (pack.mOptimalQuantity >= discountBreak.getUnits() && pack.TC < TC) {
                        mOutParams = pack;
                        TC = mOutParams.TC;
                    }


                    pack = calculate(params, discountBreak.getUnits());

                    if (pack.TC < TC) {
                        mOutParams = pack;
                        TC = mOutParams.TC;
                    }

            }
        }
    }




    public static Builder newBuilder() {
        return new EOQ().new Builder();
    }
    public Builder builderFromThis() { return new EOQ().new Builder(this); }
    public Builder builderFromThisWithDiscounts() { return new EOQ().new Builder(this).setDiscountBreaks((ArrayList<DiscountBreak>) this.mDiscountBreaks.clone()); }

    public class Builder {

        private Builder() {
            EOQ.this.mInParams = new InParameters();
            EOQ.this.mOutParams = new OutParameters();
            EOQ.this.mInParams.mOpportunityCost = 0.0f;
            EOQ.this.mInParams.mReplenishmentRatePerYear = Float.POSITIVE_INFINITY;
            EOQ.this.mInParams.mUnitShortageCostPerYear = Float.POSITIVE_INFINITY;
        }

        private Builder(EOQ old) {
            EOQ.this.mInParams = new InParameters(old.mInParams);
            EOQ.this.mOutParams = new OutParameters();
        }

        public EOQ build() {
            EOQ.this.calculateEOQ();
            return EOQ.this;
        }
        public Builder setDemandPerYear(int demand) {
            if(demand < 0) {
                throw new InvalidParameterException("Demand cannot be negative.");
            }
            EOQ.this.mInParams.mDemandPerYear = demand;
            return this;
        }

        public Builder setOrderCostPerOrder(double setupCost) {
            if(setupCost < 0) {
                throw new InvalidParameterException("Order cost cannot be negative.");
            }
            EOQ.this.mInParams.mOrderCost = setupCost;
            return this;
        }
        public Builder setUnitHoldingCostPerYear(double cost) {
            if(cost < 0) {
                throw new InvalidParameterException("Unit holding cost cannot be negative.");
            }
            EOQ.this.mInParams.mUnitHoldingCostPerYear = cost;
            return this;
        }

        public Builder setUnitShortageCostPerYear(double cost) {
            if(cost < 0) {
                throw new InvalidParameterException("Unit shortage cost cannot be negative.");
            }
            EOQ.this.mInParams.mUnitShortageCostPerYear = cost;
            return this;
        }

        public Builder setReplenishmentRatePerYear(double replenishmentRatePerYear) {
            if(replenishmentRatePerYear < 0) {
                throw new InvalidParameterException("Replenishment rate cannot be negative");
            }
            EOQ.this.mInParams.mReplenishmentRatePerYear = replenishmentRatePerYear;
            return this;
        }

        public Builder setLeadTime(double leadTime) {
            if(leadTime < 0 || leadTime > 1) {
                throw new InvalidParameterException("Lead time should be in range 0-1");
            }
            EOQ.this.mInParams.mLeadTime = leadTime;
            return this;
        }

        public Builder setUnitCostWithoutDiscount(double cost) {
            if(cost < 0) {
                throw new InvalidParameterException("Cost cannot be negative");
            }
            EOQ.this.mInParams.mCost = cost;
            return this;
        }

        public Builder setDiscountBreaks(ArrayList<DiscountBreak> discountBreaks) {
            mDiscountBreaks = discountBreaks;
            return this;
        }

        public Builder setOptimalQuantity(double quantity) {
            if(quantity < 0) {
                throw new InvalidParameterException("Optimal quantity cannot be negative");
            }
            mOutParams.mOptimalQuantity = quantity;
            return this;
        }

    }

    public static class OutParameters implements Serializable{
        public double mOptimalQuantity = Double.NaN;
        public double mMaximumInventory = 0.0f;
        public double mMaximumBackorder = 0.0f;
        public double mOrderIntervalPerYear = 0.0f;
        public double mROP = 0.0f;
        public double mTotalOrderingCost = 0.0f;
        public double mTotalHoldingCost = 0.0f;
        public double mTotalShortageCost = 0.0f;
        public double mTotalMaterialCost = 0.0f;
        public double TC = 0.0f;

        public OutParameters() { }

        private static double calculateQuantityWithDeficitAndReplenishment(InParameters params) {
            double firstArg = 2 * params.mOrderCost* params.mDemandPerYear / (params.mUnitHoldingCostPerYear* (1 - params.mDemandPerYear / params.mReplenishmentRatePerYear));
            double secondArg = Math.pow(params.mOpportunityCost * params.mDemandPerYear, 2) / (params.mUnitHoldingCostPerYear * (params.mUnitHoldingCostPerYear + params.mUnitShortageCostPerYear));
            double thirdArg = (params.mUnitHoldingCostPerYear + params.mUnitShortageCostPerYear) / params.mUnitShortageCostPerYear;
            return Math.sqrt((firstArg - secondArg) * thirdArg);
        }

        public static OutParameters calculateWithNoDeficitAndReplenishmentRate(InParameters params, double optimalQuantity) {
            OutParameters pack = new OutParameters();
            pack.mOptimalQuantity = optimalQuantity;
            pack.mMaximumInventory = (params.mReplenishmentRatePerYear - params.mDemandPerYear) * pack.mOptimalQuantity / params.mReplenishmentRatePerYear;
            pack.mMaximumBackorder = 0;
            pack.mOrderIntervalPerYear = pack.mOptimalQuantity / params.mDemandPerYear;

            if(params.mLeadTime > pack.mOptimalQuantity/params.mDemandPerYear - pack.mOptimalQuantity/params.mReplenishmentRatePerYear)
            {
                throw new InvalidParameterException("Invalid ROP");
            }
            pack.mROP = params.mDemandPerYear * params.mLeadTime;
            pack.mTotalOrderingCost = params.mDemandPerYear / pack.mOptimalQuantity * params.mOrderCost;
            pack.mTotalHoldingCost = (params.mReplenishmentRatePerYear - params.mDemandPerYear) * pack.mOptimalQuantity/(2*params.mReplenishmentRatePerYear) * params.mUnitHoldingCostPerYear;
            pack.mTotalShortageCost = 0;
            pack.mTotalMaterialCost = params.mDemandPerYear * params.mCost;
            pack.TC = pack.mTotalHoldingCost + pack.mTotalOrderingCost + pack.mTotalMaterialCost;
            return pack;
        }

        public static OutParameters calculateWithNoDeficitAndReplenishmentRate(InParameters params) {
            double K3 = Math.sqrt(params.mReplenishmentRatePerYear / (params.mReplenishmentRatePerYear - params.mDemandPerYear));
            double optimalQuantity = Math.sqrt(2* params.mOrderCost * params.mDemandPerYear/(params.mUnitHoldingCostPerYear)) * K3;
            return calculateWithNoDeficitAndReplenishmentRate(params, optimalQuantity);
        }

        public static OutParameters calculateWithDeficitAndReplenishmentRate(InParameters params, double optimalQuantity) {
            OutParameters pack = new OutParameters();

            pack.mOptimalQuantity = optimalQuantity;

            //TODO: clarify if opportunity cost is the same as unit shortage cost independent of time
            pack.mMaximumBackorder = (params.mUnitHoldingCostPerYear * pack.mOptimalQuantity - params.mOpportunityCost * params.mDemandPerYear)*(1-params.mDemandPerYear / params.mReplenishmentRatePerYear) / (params.mUnitHoldingCostPerYear + params.mUnitShortageCostPerYear);
            pack.mMaximumInventory = pack.mOptimalQuantity * (1 - params.mDemandPerYear / params.mReplenishmentRatePerYear) - pack.mMaximumBackorder;
            pack.mOrderIntervalPerYear = pack.mOptimalQuantity / params.mDemandPerYear;
            if(params.mLeadTime > pack.mOptimalQuantity / params.mDemandPerYear - pack.mOptimalQuantity/params.mReplenishmentRatePerYear)
            {
                throw new InvalidParameterException("Invalid ROP: lead time is less than time of cycle - production rate.");
            }
            pack.mROP = -pack.mMaximumBackorder + params.mDemandPerYear * params.mLeadTime;
            pack.mTotalOrderingCost = params.mDemandPerYear / pack.mOptimalQuantity * params.mOrderCost;
            pack.mTotalHoldingCost = params.mUnitHoldingCostPerYear * Math.pow(pack.mOptimalQuantity * (1-params.mDemandPerYear/params.mReplenishmentRatePerYear) - pack.mMaximumBackorder, 2) / (2*pack.mOptimalQuantity*(1-params.mDemandPerYear/params.mReplenishmentRatePerYear));
            pack.mTotalShortageCost = params.mUnitShortageCostPerYear * Math.pow(pack.mMaximumBackorder ,2) / (2*pack.mOptimalQuantity*(1- params.mDemandPerYear/params.mReplenishmentRatePerYear)) + params.mOpportunityCost * pack.mMaximumBackorder * params.mDemandPerYear/pack.mOptimalQuantity;
            //TODO: opportunity cost?? or is it material cost?
            pack.mTotalMaterialCost = params.mDemandPerYear * params.mCost;
            pack.TC = pack.mTotalHoldingCost + pack.mTotalOrderingCost + pack.mTotalMaterialCost + pack.mTotalShortageCost;
            return pack;
        }
        public static OutParameters calculateWithDeficitAndReplenishmentRate(InParameters params) {
            double optimalQuantity = calculateQuantityWithDeficitAndReplenishment(params);
            return calculateWithDeficitAndReplenishmentRate(params, optimalQuantity);
        }

        public static OutParameters calculateWithDeficitAndInstanteneousReplenishment(InParameters params) {
            double K1 = Math.sqrt((params.mUnitShortageCostPerYear + params.mUnitHoldingCostPerYear) / params.mUnitShortageCostPerYear);
            double optimalQuantity = Math.sqrt(2* params.mOrderCost * params.mDemandPerYear/(params.mUnitHoldingCostPerYear)) * K1;
            return calculateWithDeficitAndInstanteneousReplenishment(params, optimalQuantity);
        }
        public static OutParameters calculateWithDeficitAndInstanteneousReplenishment(InParameters params, double optimalQuantity) {
            double K2 = Math.sqrt(params.mUnitShortageCostPerYear/(params.mUnitShortageCostPerYear + params.mUnitHoldingCostPerYear));
            OutParameters pack = new OutParameters();

            pack.mOptimalQuantity = optimalQuantity;

            pack.mMaximumInventory = pack.mOptimalQuantity * (params.mUnitShortageCostPerYear / (params.mUnitHoldingCostPerYear + params.mUnitShortageCostPerYear));
            pack.mMaximumBackorder = pack.mOptimalQuantity - pack.mMaximumInventory;

            pack.mOrderIntervalPerYear = pack.mOptimalQuantity / params.mDemandPerYear;
            pack.mROP = -pack.mMaximumBackorder + params.mDemandPerYear * params.mLeadTime;
            pack.mTotalOrderingCost = params.mDemandPerYear / pack.mOptimalQuantity * params.mOrderCost;
            pack.mTotalHoldingCost = params.mUnitHoldingCostPerYear * Math.pow(pack.mMaximumInventory, 2) / (2 * pack.mOptimalQuantity);
            pack.mTotalShortageCost = params.mUnitShortageCostPerYear * Math.pow((pack.mMaximumBackorder), 2) / (2 * pack.mOptimalQuantity);
            pack.mTotalMaterialCost = params.mDemandPerYear * params.mCost;
            pack.TC = pack.mTotalHoldingCost + pack.mTotalOrderingCost + pack.mTotalMaterialCost + pack.mTotalShortageCost;
            return pack;
        }

        public static OutParameters calculateWithNoDeficitAndInstanteneousReplenishment(InParameters params) {
            double optimalQuantity = Math.sqrt(2* params.mOrderCost * params.mDemandPerYear/params.mUnitHoldingCostPerYear);
            return calculateWithNoDeficitAndInstanteneousReplenishment(params, optimalQuantity);
        }
        public static OutParameters calculateWithNoDeficitAndInstanteneousReplenishment(InParameters params, double optimalQuantity) {
            OutParameters pack = new OutParameters();
            pack.mOptimalQuantity = optimalQuantity;
            pack.mMaximumInventory = pack.mOptimalQuantity;
            pack.mMaximumBackorder = 0;
            pack.mOrderIntervalPerYear = pack.mOptimalQuantity / params.mDemandPerYear;
            pack.mROP = params.mDemandPerYear * params.mLeadTime;
            pack.mTotalOrderingCost = params.mDemandPerYear / pack.mOptimalQuantity * params.mOrderCost;
            pack.mTotalHoldingCost = params.mUnitHoldingCostPerYear * pack.mMaximumInventory / 2;
            pack.mTotalShortageCost = 0;
            pack.mTotalMaterialCost = params.mDemandPerYear * params.mCost;
            pack.TC = pack.mTotalHoldingCost + pack.mTotalOrderingCost + pack.mTotalMaterialCost + pack.mTotalShortageCost;
            return pack;
        }
    }

    public static class DiscountBreak implements Serializable{
        private int mUnits;
        private double mDiscount;

        public int getUnits() {
            return mUnits;
        }

        public double getDiscount() {
            return mDiscount;
        }

        public DiscountBreak(int units, double discount) {
            mUnits = units;
            if(discount > 100 || discount < 0) {
                throw new InvalidParameterException("Discount must be in a range 0-100.");
            }
            mDiscount = discount;
        }


    }
}
