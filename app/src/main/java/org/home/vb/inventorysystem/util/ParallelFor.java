package org.home.vb.inventorysystem.util;

import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ParallelFor<Return> {

    ExecutorService mService;
    Object mResult[];

    int mCoresNum;
    int mIterationsPerThread;
    int mMaximumIterations;
    int mBuckets;

    public ParallelFor(int maximumIterations) {
        mCoresNum = Math.max(Runtime.getRuntime().availableProcessors(), 2) - 1;
        mService = Executors.newFixedThreadPool(mCoresNum);
        mIterationsPerThread = (int) Math.ceil(maximumIterations / (float)mCoresNum);


        if(mIterationsPerThread == 0 && maximumIterations != 0) {
            mIterationsPerThread = 1;
        }

        mMaximumIterations = maximumIterations;
        mBuckets = mMaximumIterations / mIterationsPerThread;
        mResult = new Object[mBuckets];
    }

    public Return process(final UnaryFunction<Integer, Return> functionOnIteration, final BinaryFunction<Return, Return, Return> functionReduce) throws InterruptedException {
        for(int bucket = 0; bucket < mBuckets; bucket++) {
            final int bucketIndex = bucket;
            final int startValue = bucket * mIterationsPerThread;
            final int endValue = startValue + mIterationsPerThread;

            mService.execute(new Runnable() {
                @Override
                public void run() {
                    final int end = Math.min(endValue, mMaximumIterations);
                    for (int i = startValue; i < end; i++) {
                        mResult[bucketIndex] = functionReduce.apply(functionOnIteration.apply(i), (Return) mResult[bucketIndex]);
                    }
                }
            });
        }
        mService.shutdown();
        mService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

        if(mResult == null) {
            Log.d("TAG", "BUckets: " + mBuckets);
        }
        Return res = null;
        for(int i = 0; i < mBuckets; ++i ){
            res = functionReduce.apply((Return) mResult[i], res);
        }

        return res;
    }
}


