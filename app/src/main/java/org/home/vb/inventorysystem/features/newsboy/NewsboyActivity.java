package org.home.vb.inventorysystem.features.newsboy;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import org.home.vb.inventorysystem.R;
import org.home.vb.inventorysystem.util.SdkHelpers;

public class NewsboyActivity extends AppCompatActivity {

    TextInputLayout meanInputLayout;
    TextInputLayout stdDevInputLayout;
    TextInputLayout orderCostInputLayout;
    TextInputLayout unitAcqCostInputLayout;
    TextInputLayout sellingPriceInputLayout;
    TextInputLayout unitOpportunityCostInputLayout;
    TextInputLayout unitSalvageCostInputLayout;
    Spinner distributionSpinner;
    Button calculateBtn;


   private void findViews() {
       meanInputLayout = (TextInputLayout) findViewById(R.id.input_layout_mean);
       stdDevInputLayout = (TextInputLayout) findViewById(R.id.input_layout_std_dev);
       orderCostInputLayout = (TextInputLayout) findViewById(R.id.input_layout_order_cost_newsboy);
       unitAcqCostInputLayout = (TextInputLayout) findViewById(R.id.input_layout_unit_cost_newsboy);
       sellingPriceInputLayout = (TextInputLayout) findViewById(R.id.input_layout_unit_selling_price);
       unitOpportunityCostInputLayout = (TextInputLayout) findViewById(R.id.input_layout_unit_opportunity_cost);
       unitSalvageCostInputLayout = (TextInputLayout) findViewById(R.id.input_layout_unit_salvage_value);
       distributionSpinner = (Spinner) findViewById(R.id.distribution_spinner);
       calculateBtn = (Button) findViewById(R.id.button_newsboy_calculate);
   }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        else if(item.getItemId() == R.id.menu_item_test_set) {
            meanInputLayout.getEditText().setText("4000");
            stdDevInputLayout.getEditText().setText("1000");
            orderCostInputLayout.getEditText().setText("0");
            unitAcqCostInputLayout.getEditText().setText("17");
            sellingPriceInputLayout.getEditText().setText("30");
            unitOpportunityCostInputLayout.getEditText().setText("0");
            unitSalvageCostInputLayout.getEditText().setText("10");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.newsboy_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsboy);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        findViews();
        String[] items = {"Normal"}; //, "Poisson"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, items);
        distributionSpinner.setAdapter(adapter);

        calculateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Newsboy.Builder builder = Newsboy.newBuilder();

                Double mean  = SdkHelpers.getDoubleIfNotNull(meanInputLayout.getEditText());
                if(mean != null) {
                    builder.setMean(mean);
                    meanInputLayout.setErrorEnabled(false);
                }
                else {
                    meanInputLayout.setError("Mean is empty");
                    return;
                }

                Double stdDev = SdkHelpers.getDoubleIfNotNull(stdDevInputLayout.getEditText());
                if(stdDev != null) {
                    builder.setStdDev(stdDev);
                    stdDevInputLayout.setErrorEnabled(false);
                }
                else {
                    stdDevInputLayout.setError("Std. deviation is empty");
                    return;
                }

                Double orderCost = SdkHelpers.getDoubleIfNotNull(orderCostInputLayout.getEditText());
                if(orderCost != null) {
                    builder.setOrderCost(orderCost);
                }

                Double acqCost = SdkHelpers.getDoubleIfNotNull(unitAcqCostInputLayout.getEditText());
                if(acqCost != null) {
                    builder.setAcqCost(acqCost);
                    unitAcqCostInputLayout.setErrorEnabled(false);
                }
                else {
                    unitAcqCostInputLayout.setError("Unit acquisition cost is empty");
                    return;
                }

                Double sellingPrice = SdkHelpers.getDoubleIfNotNull(sellingPriceInputLayout.getEditText());
                if(sellingPrice != null) {
                    builder.setUnitSellingPrice(sellingPrice);
                    sellingPriceInputLayout.setErrorEnabled(false);
                }
                else {
                    sellingPriceInputLayout.setError("Selling price is empty");
                    return;
                }

                Double unitOpportunityCost = SdkHelpers.getDoubleIfNotNull(unitOpportunityCostInputLayout.getEditText());
                if(unitOpportunityCost != null) {
                    builder.setShortageCost(unitOpportunityCost);
                }

                Double salvageValue = SdkHelpers.getDoubleIfNotNull(unitSalvageCostInputLayout.getEditText());
                if(salvageValue != null) {
                    builder.setSalvageValue(salvageValue);
                }
                Newsboy newsboy = null;
                try {
                    newsboy = builder.build();
                } catch (InterruptedException e) {
                    SdkHelpers.showErrorNotification(NewsboyActivity.this, e.getMessage());
                }
                Intent intent = NewsboyResultActivity.createIntent(NewsboyActivity.this, newsboy);
                startActivity(intent);
            }
        });


    }
}
