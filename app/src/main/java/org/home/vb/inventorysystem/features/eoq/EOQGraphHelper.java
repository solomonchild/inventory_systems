package org.home.vb.inventorysystem.features.eoq;


import android.support.v4.util.Pair;

import org.home.vb.inventorysystem.features.eoq.EOQ;

import java.util.ArrayList;
import java.util.List;

public class EOQGraphHelper {

    private static Pair<Point, Point> getLineDecline(EOQ eoq, float x) {
        Pair<Point, Point> line = new Pair<>(new Point(), new Point());
        if(eoq.isDeficitAllowed() && !eoq.isReplenishmentInstantaneous()) {
            float tau1 = (float) (eoq.getMaximumInventory() / eoq.getDemandPerYear());
            line.first.setX(x);
            line.first.setY((float) eoq.getMaximumInventory());
            line.second.setX((float) (x + (eoq.getOrderIntervalPerYear() - tau1)));
            line.second.setY((float) -eoq.getMaximumBackorder());
        }
        else if(eoq.isReplenishmentInstantaneous() && !eoq.isDeficitAllowed()) {
            line.first.setX(x);
            line.first.setY((float) eoq.getOptimalQuantity());
            line.second.setX((float) (x + eoq.getOrderIntervalPerYear()));
            line.second.setY(0);
        }
        else if(!eoq.isReplenishmentInstantaneous()) {
            line.first.setX(x);
            line.first.setY((float) eoq.getMaximumInventory());

            float tau = (float) (eoq.getOptimalQuantity() / eoq.getReplenishmentRatePerYear());
            line.second.setX((float) (x + (eoq.getOrderIntervalPerYear() - tau)));
            line.second.setY(0);
        }
        else if(eoq.isDeficitAllowed()) {
            line.first.setX(x);
            line.first.setY((float) eoq.getMaximumInventory());
            line.second.setX((float) (x + eoq.getOrderIntervalPerYear()));
            line.second.setY((float) -eoq.getMaximumBackorder());
        }
        return line;
    }

    private static Pair<Point, Point> getLineRise(EOQ eoq, float x) {
       Pair<Point, Point> line = new Pair<>(new Point(), new Point());
        if(eoq.isDeficitAllowed() && !eoq.isReplenishmentInstantaneous()) {
            float tau2 = (float) (eoq.getOptimalQuantity() / eoq.getReplenishmentRatePerYear());
            line.first.setX(x);
            line.first.setY((float) -eoq.getMaximumBackorder());
            line.second.setX(x + tau2);
            line.second.setY((float) eoq.getMaximumInventory());
        }
        else if(eoq.isReplenishmentInstantaneous() && !eoq.isDeficitAllowed()) {
            line.first.setX(x);
            line.first.setY(0);
            line.second.setX(x);
            line.second.setY((float)eoq.getOptimalQuantity());
        }
        else if(!eoq.isReplenishmentInstantaneous()) {
            line.first.setX(x);
            line.first.setY(0);

            float tau = (float) (eoq.getOptimalQuantity() / eoq.getReplenishmentRatePerYear());
            line.second.setX(x + tau);
            line.second.setY((float) eoq.getMaximumInventory());
        }
        else if(eoq.isDeficitAllowed()) {
            line.first.setX(x);
            line.first.setY((float) -eoq.getMaximumBackorder());
            line.second.setX(x);
            line.second.setY((float) eoq.getMaximumInventory());
        }
        return line;
    }

    public static List<Pair<Float, Float>> getPoints(EOQ eoq) {
        List<Pair<Float, Float>> series = new ArrayList<>();
        int cycles = (int) (1/eoq.getOrderIntervalPerYear());

        float prevX = 0;
        Pair<Point, Point> line;
        if(eoq.isDeficitAllowed()) {
            line = getLineDecline(eoq, 0);
            series.add(new Pair(line.first.getX(), line.first.getY()));
            series.add(new Pair(line.second.getX(), line.second.getY()));
            prevX = line.second.getX();
            cycles --;
        }
        else if(!eoq.isReplenishmentInstantaneous()) {
            line = getLineRise(eoq, 0);
            series.add(new Pair(line.first.getX(), line.first.getY()));
        }
        for(int j = 0; j < cycles; ++j) {
            line = getLineRise(eoq, prevX);
            series.add(new Pair(line.second.getX(), line.second.getY()));
            line = getLineDecline(eoq, line.second.getX());
            series.add(new Pair(line.second.getX(), line.second.getY()));
            prevX = line.second.getX();
        }
        return series;
    }

    public static class Point {

        float mX = 0.0f;
        float mY = 0.0f;

        public Point() {
        }

        public void setX(float x) {
            mX = x;
        }

        public void setY(float y) {
            mY = y;
        }

        public float getY() {

            return mY;
        }

        public float getX() {
            return mX;
        }

    }
}
