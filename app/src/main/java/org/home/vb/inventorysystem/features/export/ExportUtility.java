package org.home.vb.inventorysystem.features.export;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;

import org.home.vb.inventorysystem.R;

import java.util.ArrayList;

import static android.provider.Contacts.ContactMethods.CONTENT_EMAIL_TYPE;

public class ExportUtility extends AppCompatActivity {


    public static void export(ArrayList<Uri> dataUris, ArrayList<Uri> graphUris, Context context) {
            Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            intent.setType(CONTENT_EMAIL_TYPE);
            ArrayList<Uri> resultingUris = new ArrayList<>();
            resultingUris.addAll(graphUris);
            resultingUris.addAll(dataUris);
            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, resultingUris);

            Intent chooser = Intent.createChooser(intent, context.getString(R.string.export_results_to));
            context.startActivity(chooser);
    }
}
