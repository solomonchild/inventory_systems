package org.home.vb.inventorysystem.features.newsboy;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.apache.commons.math3.exception.OutOfRangeException;
import org.home.vb.inventorysystem.features.export.ExportUtility;
import org.home.vb.inventorysystem.util.BinaryFunction;
import org.home.vb.inventorysystem.util.Formatters;
import org.home.vb.inventorysystem.R;
import org.home.vb.inventorysystem.databinding.ActivityNewsboyResultBinding;
import org.home.vb.inventorysystem.util.SdkHelpers;
import org.home.vb.inventorysystem.util.UnaryFunction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewsboyResultActivity extends AppCompatActivity {

    private static String NEWSBOY_EXTRA_ID = "NEWSBOY_EXTRA";

    Newsboy mNewsboy;
    Newsboy mOriginalNewsboy;
    LineChart mGraph;
    TextView mPropertySelectorTextView;
    AppCompatSpinner mPropertySelectorSpinner;
    SeekBar mPropertySelectorSeekBar;
    Button mResetButton;
    ActivityNewsboyResultBinding mBinding;

    public static Intent createIntent(Context context, Newsboy newsboy) {
        Intent intent = new Intent(context, NewsboyResultActivity.class);
        intent.putExtra(NEWSBOY_EXTRA_ID, newsboy);
        return intent;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.export_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_item_export) {
            ArrayList<Uri> dataUris = new ArrayList<>();
            ArrayList<Uri> graphUris = new ArrayList<>();
            Map<String, String> values = new HashMap<>();
            values.put("Optimal Expected Profit", String.valueOf(mNewsboy.getOptimalExpectedProfit()));
            values.put("Optimal Order Quantity", String.valueOf(mNewsboy.getOptimalOrderQuantity()));
            values.put("Optimal Service Level", String.valueOf(mNewsboy.getOptimalServiceLevel()));
            try {
                graphUris.add(SdkHelpers.saveBitmapToFile(this, mGraph.getChartBitmap(), "newsboy_graph"));
                dataUris.add(SdkHelpers.saveMapToCsv(this, values, "newsboy_result"));

            } catch (IOException e) {
                SdkHelpers.showErrorNotification(this, e.getMessage());
                return super.onOptionsItemSelected(item);
            }
            ExportUtility.export(dataUris, graphUris, this);
            return true;
        }
        else if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    void preSetupGraph() {
        final double percentOfAfter = 0.7;
        Description desc = new Description();
        desc.setText("Service Level");
        mGraph.setDescription(desc);
        mGraph.getXAxis().setDrawAxisLine(true);
        mGraph.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mGraph.getXAxis().setAxisMinimum(0);
        mGraph.getXAxis().setAxisMaximum((float)(mNewsboy.getOptimalServiceLevel() * (1 + percentOfAfter)));
        mGraph.getXAxis().setValueFormatter(new Formatters.AxisFormatter());
        mGraph.getXAxis().setLabelCount(10);

        mGraph.getAxisRight().setDrawLabels(false);
        mGraph.getAxisRight().setValueFormatter(new Formatters.AxisFormatter());
        mGraph.getAxisRight().setAxisMinimum(38000);
        mGraph.getAxisRight().setDrawZeroLine(true);

    }

    void setupGraph() {
        final int numberOfPoints = 13;
        double start = mNewsboy.getOptimalServiceLevel() * 0.3;
        double step = (1 - start) / numberOfPoints;
        List<Entry> expectedProfitEntries = new ArrayList<>();

        if (!Double.isNaN(mNewsboy.getOptimalServiceLevel())) {
            for (int i = 1; i < numberOfPoints; ++i) {

                Newsboy.Builder newBuilder = mNewsboy.builderFromThis();
                newBuilder.setOptimalServiceLevel(start + step * i);
                newBuilder.setOptimalOrderQuantity(Double.NaN);
                Newsboy newNewsboy = null;
                try {
                    newNewsboy = newBuilder.build();
                } catch (InterruptedException e) {
                    SdkHelpers.showErrorNotification(NewsboyResultActivity.this, e.getMessage());
                    return;
                }
                expectedProfitEntries.add(new Entry((float) newNewsboy.getOptimalServiceLevel(), (float) newNewsboy.getOptimalExpectedProfit()));
            }

            LineDataSet expectedProfitDataSet = new LineDataSet(expectedProfitEntries, "Expected optimal profit");

            LineData lineData = new LineData();
            lineData.addDataSet(expectedProfitDataSet);


            lineData.setValueFormatter(new Formatters.ValueFormatter());
            mGraph.setData(lineData);
            lineData.setValueTextSize(13);
            mGraph.setMaxVisibleValueCount(7);

            LimitLine ll = new LimitLine((float)mNewsboy.getOptimalServiceLevel(), "Optimal Service Level");
            ll.setTextSize(8);
            mGraph.getXAxis().removeAllLimitLines();
            mGraph.getXAxis().addLimitLine(ll);

            mGraph.invalidate();
        }
    }

    void onNewsboyChanged() {
        mBinding.setNewsboy(mNewsboy);
        mBinding.executePendingBindings();
        setupGraph();
    }
    void setupSeekBar(final String item, UnaryFunction<Newsboy, Double> getter, final BinaryFunction<Integer, Newsboy.Builder, Newsboy.Builder> setter) {

        double fieldValue = getter.apply(mNewsboy);
        double maxValue = fieldValue == 0 ? 9999 : fieldValue * 2;
        mPropertySelectorTextView.setText(String.valueOf(fieldValue));
        mPropertySelectorSeekBar.setOnSeekBarChangeListener(null);
        mPropertySelectorSeekBar.setProgress(0);
        mPropertySelectorSeekBar.setMax((int) maxValue);
        mPropertySelectorSeekBar.setProgress((int) fieldValue);
        mPropertySelectorSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mPropertySelectorTextView.setText(String.valueOf(progress));
                try {
                    Newsboy.Builder builder = mNewsboy.builderFromThis();
                    mNewsboy = setter.apply(progress, builder)
                            .setOptimalServiceLevel(Double.NaN)
                            .setOptimalOrderQuantity(Double.NaN)
                            .build();
                    onNewsboyChanged();
                }
                catch(OutOfRangeException e) {
                    String message = new String("Invalid value for " + item + e.getMessage());
                    SdkHelpers.showErrorNotification(NewsboyResultActivity.this, message);
                }
                catch (Exception e) {
                    SdkHelpers.showErrorNotification(NewsboyResultActivity.this, e.getMessage());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    void setupSpinner() {

        final String[] items = {
                getResources().getString(R.string.mean),
                getResources().getString(R.string.std_dev),
                getResources().getString(R.string.order_cost),
                getResources().getString(R.string.unit_acquisition_cost),
                getResources().getString(R.string.unit_selling_price),
                getResources().getString(R.string.unit_opportunity_cost),
                getResources().getString(R.string.unit_salvage_value),
        };
        final BinaryFunction<Integer, Newsboy.Builder, Newsboy.Builder>[] binaryFunctions = new BinaryFunction[] {
                new BinaryFunction<Integer, Newsboy.Builder, Newsboy.Builder>() {
                    @Override
                    public Newsboy.Builder apply(Integer t1, Newsboy.Builder t2) {
                        t2.setMean(t1);
                        return t2;
                    }
                },
                new BinaryFunction<Integer, Newsboy.Builder, Newsboy.Builder>() {
                    @Override
                    public Newsboy.Builder apply(Integer t1, Newsboy.Builder t2) {
                        t2.setStdDev(t1);
                        return t2;
                    }
                },
                new BinaryFunction<Integer, Newsboy.Builder, Newsboy.Builder>() {
                    @Override
                    public Newsboy.Builder apply(Integer t1, Newsboy.Builder t2) {
                        t2.setOrderCost(t1);
                        return t2;
                    }
                },
                new BinaryFunction<Integer, Newsboy.Builder, Newsboy.Builder>() {
                    @Override
                    public Newsboy.Builder apply(Integer t1, Newsboy.Builder t2) {
                        t2.setAcqCost(t1);
                        return t2;
                    }
                },
                new BinaryFunction<Integer, Newsboy.Builder, Newsboy.Builder>() {
                    @Override
                    public Newsboy.Builder apply(Integer t1, Newsboy.Builder t2) {
                        t2.setUnitSellingPrice(t1);
                        return t2;
                    }
                },
                new BinaryFunction<Integer, Newsboy.Builder, Newsboy.Builder>() {
                    @Override
                    public Newsboy.Builder apply(Integer t1, Newsboy.Builder t2) {
                        t2.setShortageCost(t1);
                        return t2;
                    }
                },
                new BinaryFunction<Integer, Newsboy.Builder, Newsboy.Builder>() {
                    @Override
                    public Newsboy.Builder apply(Integer t1, Newsboy.Builder t2) {
                        t2.setSalvageValue(t1);
                        return t2;
                    }
                },



        };
        final UnaryFunction<Newsboy, Double>[] unaryFunctions = new UnaryFunction[] {
                new UnaryFunction<Newsboy, Double>() {
                    @Override
                    public Double apply(Newsboy param) {
                        return param.getMean();
                    }
                },
                new UnaryFunction<Newsboy, Double>() {
                    @Override
                    public Double apply(Newsboy param) {
                        return param.getStdDev();
                    }
                },
                new UnaryFunction<Newsboy, Double>() {
                    @Override
                    public Double apply(Newsboy param) {
                        return param.getOrderCost();
                    }
                },
                new UnaryFunction<Newsboy, Double>() {
                    @Override
                    public Double apply(Newsboy param) {
                        return param.getAcqCost();
                    }
                },

                new UnaryFunction<Newsboy, Double>() {
                    @Override
                    public Double apply(Newsboy param) {
                        return param.getUnitSellingPrice();
                    }
                },
                new UnaryFunction<Newsboy, Double>() {
                    @Override
                    public Double apply(Newsboy param) {
                        return param.getShortageCost();
                    }
                },
                new UnaryFunction<Newsboy, Double>() {
                    @Override
                    public Double apply(Newsboy param) {
                        return param.getSalvageValue();
                    }
                },


        };
       ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, items);
       mPropertySelectorSpinner.setAdapter(adapter);
        mPropertySelectorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position >= 0 && position < items.length){
                    setupSeekBar(items[position], unaryFunctions[position], binaryFunctions[position]);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        preSetupGraph();
        setupGraph();
        setupSpinner();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsboy_result);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mNewsboy = (Newsboy) getIntent().getSerializableExtra(NEWSBOY_EXTRA_ID);
        mOriginalNewsboy = mNewsboy.clone();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_newsboy_result);
        mBinding.setNewsboy(mNewsboy);
        mGraph = (LineChart) findViewById(R.id.newsboy_graph);
        mPropertySelectorTextView = (TextView) findViewById(R.id.text_view_property_selector_newsboy);
        mPropertySelectorSpinner = (AppCompatSpinner) findViewById(R.id.spinner_property_selector_newsboy);
        mPropertySelectorSeekBar = (SeekBar) findViewById(R.id.seek_bar_property_selector_newsboy);
        mResetButton = (Button) findViewById(R.id.button_reset_newsboy);
        mResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            mNewsboy = mOriginalNewsboy.clone();
                onNewsboyChanged();
              mPropertySelectorSpinner.postDelayed(new Runnable() {
                  @Override
                  public void run() {
                      int selectedPosition = mPropertySelectorSpinner.getSelectedItemPosition();
                      int next = (selectedPosition +1) % mPropertySelectorSpinner.getAdapter().getCount();
                      mPropertySelectorSpinner.setSelection(next, true);
                      mPropertySelectorSpinner.setSelection(selectedPosition, true);
                  }
              }, 0);
            }
        });
    }
}
