package org.home.vb.inventorysystem.features.eoq;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.home.vb.inventorysystem.R;

import java.security.InvalidParameterException;
import java.util.ArrayList;

import static org.home.vb.inventorysystem.util.SdkHelpers.getDoubleIfNotNull;


public class EOQActivity extends AppCompatActivity {

    TextInputLayout demandIL;
    TextInputLayout orderCostIL;
    TextInputLayout holdingCostIL;
    TextInputLayout shortageCostIL;
    TextInputLayout costIL;
    TextInputLayout leadTimeIL;
    TextInputLayout replenishmentIL;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        return true;
    }

    private void findViews() {
        demandIL = (TextInputLayout) findViewById(R.id.input_layout_demand_per_year);
        orderCostIL = (TextInputLayout) findViewById(R.id.input_layout_order_cost_per_order);
        holdingCostIL = (TextInputLayout) findViewById(R.id.input_layout_holding_cost_per_year);
        shortageCostIL = (TextInputLayout) findViewById(R.id.input_layout_shortage_cost_per_year);
        costIL = (TextInputLayout) findViewById(R.id.input_layout_acquisition_cost);
        leadTimeIL = (TextInputLayout) findViewById(R.id.input_layout_lead_time);
        replenishmentIL = (TextInputLayout) findViewById(R.id.input_layout_replenishment_rate);
    }

    private void addDiscountView() {
        final LinearLayout root_layout = (LinearLayout) findViewById(R.id.linear_layout_eoq_in_scroll_view);
        final ViewGroup view = (ViewGroup) getLayoutInflater().inflate(R.layout.layout_discount_row, null);
        Button removeButton = (Button) view.getChildAt(2);
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                root_layout.removeView(view);
                ArrayList<ViewGroup> views = getDiscountRows();
                if(!views.isEmpty()) {
                    views.get(0).requestFocus();
                }
            }
        });
        root_layout.addView(view);

        final ScrollView scrollView = (ScrollView) root_layout.getParent();
        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
                view.getChildAt(0).requestFocus();
            }
        }, 200);
    }

    private void fillDiscountTestData() {
        demandIL.getEditText().setText("4000");
        orderCostIL.getEditText().setText("5");
        holdingCostIL.getEditText().setText("3.4");
        leadTimeIL.getEditText().setText("0.0222");
        costIL.getEditText().setText("17");
        addDiscountView();
        addDiscountView();
        addDiscountView();
        ArrayList<ViewGroup> rows = getDiscountRows();
        ((TextInputLayout)rows.get(2).getChildAt(0)).getEditText().setText("160");
        ((TextInputLayout)rows.get(2).getChildAt(1)).getEditText().setText("0.02");
        ((TextInputLayout)rows.get(1).getChildAt(0)).getEditText().setText("200");
        ((TextInputLayout)rows.get(1).getChildAt(1)).getEditText().setText("0.03");
        ((TextInputLayout)rows.get(0).getChildAt(0)).getEditText().setText("220");
        ((TextInputLayout)rows.get(0).getChildAt(1)).getEditText().setText("0.05");

    }

    private void fillDataSet() {
        demandIL.getEditText().setText("3600");
        orderCostIL.getEditText().setText("200");
        holdingCostIL.getEditText().setText("25");
        shortageCostIL.getEditText().setText("40");
        leadTimeIL.getEditText().setText("0.05");
        costIL.getEditText().setText("100");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
            case R.id.menu_item_add_discount:
                addDiscountView();
                break;
            case R.id.menu_item_test_set:
                fillDataSet();
                break;
            case R.id.menu_item_test_set_discounts:
                fillDiscountTestData();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<ViewGroup> getDiscountRows() {
        ArrayList<ViewGroup> discountBreakRows = new ArrayList<>();
        LinearLayout layout = (LinearLayout) findViewById(R.id.linear_layout_eoq_in_scroll_view);
        int childCount = layout.getChildCount();
        for(int i = childCount - 1; i >= 0; --i) {
            Object tag = layout.getChildAt(i).getTag();
            if (tag != null && tag instanceof String && tag.equals("discount_row")) {
                discountBreakRows.add((ViewGroup)layout.getChildAt(i));
            }
        }
        return discountBreakRows;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eoq);
        findViews();

        Button buttonCalculate = (Button) findViewById(R.id.button_eoq_calculate);
        buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EOQ.Builder builder = EOQ.newBuilder();
                Double demand = getDoubleIfNotNull(demandIL.getEditText());
                if (demand != null) {
                    builder.setDemandPerYear((int) demand.doubleValue());
                    demandIL.setErrorEnabled(false);
                }
                else {
                    demandIL.setError("Demand is empty");
                    return;
                }

                Double orderCost = getDoubleIfNotNull(orderCostIL.getEditText());
                if(orderCost != null) {
                    builder.setOrderCostPerOrder(orderCost);
                    orderCostIL.setErrorEnabled(false);
                }
                else {
                    orderCostIL.setError("Order cost is empty");
                    return;
                }

                Double holdingCost = getDoubleIfNotNull(holdingCostIL.getEditText());
                if(holdingCost != null) {
                    builder.setUnitHoldingCostPerYear(holdingCost);
                    holdingCostIL.setErrorEnabled(false);
                }
                else {
                    holdingCostIL.setError("Holding cost is empty");
                    return;
                }

                Double shortageCost = getDoubleIfNotNull(shortageCostIL.getEditText());
                if(shortageCost != null)
                    builder.setUnitShortageCostPerYear(shortageCost);

                Double replenishmentRate = getDoubleIfNotNull(replenishmentIL.getEditText());
                if(replenishmentRate != null)
                    builder.setReplenishmentRatePerYear(replenishmentRate);

                Double leadTime = getDoubleIfNotNull(leadTimeIL.getEditText());
                if(leadTime != null)
                    builder.setLeadTime(leadTime);

                Double cost = getDoubleIfNotNull(costIL.getEditText());
                if(cost != null) {
                    builder.setUnitCostWithoutDiscount(cost);
                    costIL.setErrorEnabled(false);
                }
                else {
                    costIL.setError("Acquisition cost is empty");
                    return;
                }

                try {
                    ArrayList<EOQ.DiscountBreak> discountBreaks = new ArrayList<>();
                    ArrayList<ViewGroup>  discountBreakRows = getDiscountRows();
                    int childCount = discountBreakRows.size();
                    for(int i = childCount - 1; i >= 0; --i) {
                        ViewGroup group = discountBreakRows.get(i);
                        TextView breakText = ((TextInputLayout)group.getChildAt(0)).getEditText();
                        TextView percentText = ((TextInputLayout)group.getChildAt(1)).getEditText();
                        if(breakText.getText().toString().isEmpty() || percentText.getText().toString().isEmpty()) {
                            continue;
                        }
                        int discountBreak = Integer.parseInt(breakText.getText().toString());
                        double percentage = Double.parseDouble(percentText.getText().toString());
                        discountBreaks.add(new EOQ.DiscountBreak(discountBreak, percentage));
                    }
                    builder.setDiscountBreaks(discountBreaks);
                    EOQ eoq = builder.build();
                    if(!Double.isInfinite(eoq.getOptimalQuantity())) {
                        Intent intent = EOQResultActivity.getIntent(EOQActivity.this, eoq);
                        startActivity(intent);
                    }
                }
                catch (InvalidParameterException ex) {
                    new AlertDialog.Builder(EOQActivity.this).setTitle("Error").setMessage(ex.getMessage()).setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).create().show();
                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
