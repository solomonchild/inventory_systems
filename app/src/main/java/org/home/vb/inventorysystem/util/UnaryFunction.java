package org.home.vb.inventorysystem.util;

public interface UnaryFunction<P, Return> {
    Return apply(P param);
}
